# importer le module discord de discord.py
import discord

# On va ensuite créer un client à partir de la classe discord.Client :
client = discord.Client()

# Et pour connecter le client (notre bot) il suffit d'utiliser 
# la méthode run et de passer le token du bot à cette méthode 
# (entre guillemets bien sûr car il s'agit d'une chaîne de caractères) :
client.run("OTMyMjY1MTgwNzc0MTQ2MDg5.YeQdmw.4ZNRuXgR3Cx3VZMwgUY4IT-euHY")

"""Pour savoir quand notre bot est réellement prêt, on peut utiliser 
l'événement on_ready.
Cet événement est déclenché par Discord une fois que l'initialisation 
du bot et sa connexion se sont déroulés avec succès.
Pour créer un événement, il suffit de créer une fonction avec le même 
nom que l'événement :
"""
# Le décorateur @client.event permet d'indiquer que la fonction on_ready
# est une fonction qui doit recevoir les 
# informations envoyées lorsque l'événement est appelé par Discord.
# @client.event

# Le mot clé async définie la fonction comme une coroutine.
# Le concept de coroutine est un concept assez avancé que vous n'avez
# pas besoin de maîtriser sur le bout des doigts pour créer un bot.
# Sachez seulement que devant les fonctions qui gèrent ces événements,
# vous devez utiliser le mot clé async.
# 
#     print("Le bot est prêt !")

"""À chaque fois qu'un message est posté sur votre serveur, 
Discord va déclencher l'événement on_message.

Pour réagir à un message posté sur votre serveur, il suffit donc 
d'implémenter une fonction pour gérer cet événement, comme on 
vient de le faire avec on_ready :"""
@client.event
async def on_message(message):
    print(message.content)
    if message.content == "Ping":
        await message.channel.send("Pong")
"""On récupère dans cette fonction le message qui est envoyé par 
l'événement dans un paramètre qu'on appelle ici message 
(mais on pourrait l'appeler autrement. Il est cependant bien important 
de mettre un paramètre dans cette fonction pour pouvoir récupérer le 
message, sinon vous obtiendrez une erreur).
L'objet message possède plusieurs attributs comme content ou author 
qui permettent d'obtenir plus d'informations sur le message posté.

Si vous souhaitez par exemple afficher le contenu du message, il suffirait de faire :
@client.event
async def on_message(message):
    print(message.content)
"""