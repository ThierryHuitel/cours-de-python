""" Ce module est un exemple simple de match case"""
import sys

while True:
    action = input("Que voulez-vous faire ? ")
    match action.split():
        case ["Jouer"]:
            print("Ok, on commence le jeu")
        case ["Quitter"]:
            print("Ok bye !")
            sys.exit()
        case ["Avancer", ("Nord" | "Sud" | "Est" | "Ouest") as orientation]:
            print(f"Ok, vous avancez d'1 unité vers le {orientation}.")
        case ["Attaquer", ennemy]:
            print(f"Ok, on attaque {ennemy} !")
        case _:
            print("Veuillez entrer une commande valide...")
            