""" Les variables
 on peut stocker des choses (du texte, des nombres, etc) dans la mémoire, dans les variables
 il faut choisir pour la variable un nom qui explique bien ce qu'on va mettre dedans
 ca facilite ensuite l'écriture du code
 voici comment on entre une valeur dans une variable: """

mon_age = 16
mon_annee_de_naissance = 1969
x = 2
y = 3
ma_taille = 1.85
mon_compteur = 0
mon_compteur = mon_compteur + 1
# on entoure du texte par 2 guillemets. On peut aussi le faire avec une seule,
# mais ça pose vite problème avec les apostrophes du français
mon_juron_favori = "mes couilles"
nom_du_chat = "l'incruste"


# demander une chose à l'utilisateur
# quand on utilise la commande input() le programme s'arrête et demande à l'utilisateur de
# taper quelque chose au clavier. Comme on enregistre ce qu'il va taper dans une variable,
# on utilise souvent une commande comme ceci:
annee_de_naissance = input("quelle est ton année de naissance, Gros? ")
nombre_de_dents = input("combien t'as de dents, Mongolito? ")
x = input("Saisissez l'abscisse svp: ")
y = input("Saisissez l'ordonnée s’il vous plaît: ")
mdp = input("Entrez votre mot de passe: ")

# afficher quelque chose
# c'est la commande print() qui va nous permettre d'afficher quelque chose dans le programme
# on peut afficher directement du texte:
print("cette commande affiche simplement du texte")
# on peut afficher directement le contenu d'une variable:
print(mon_age)
# on peut aussi afficher à la fois du texte et des variable grace au f-strings:
#il suffit d'ajouter un f après la parenthèse, et mettre les variables entre accolades. Exemple:
mon_annee_de_naissance = 1969
son_prenom = "Toto"
print(f"Salut {son_prenom}, tu es né en {mon_annee_de_naissance}.")
# on peut aussi directement faire un calcul à l'intérieur des accolades. Exemples:
print(f"Bravo {son_prenom}, tu as {2022 - mon_annee_de_naissance} ans cette année.")
print(f"Bravo {son_prenom}, tu auras {(2022 - mon_annee_de_naissance) + 10} ans dans dix ans.")

# les types des variables
# les trois types de variables les plus fréquents sont :
# les entiers numériques, ou integer (int)
# les décimaux: float
# les textes (string en anglais, ce qui veut dire chaîne de caractères): str
# python reconnait tout seul le type des variables qu'on entre
# mais parfois il faut lui demander de convertir un texte en nombre, ou l'inverse
# pour convertir un texte en entier:
ma_variable_au_format_string = "2"
ma_variable_au_format_integer = int(ma_variable_au_format_string)
# pour convertir un entier en texte:
ma_variable_au_format_integer = 2
ma_variable_au_format_string = str(ma_variable_au_format_integer)

