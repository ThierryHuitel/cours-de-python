""" calcul de l'age avec exception catching """

s = input("Quel est ton année de naissance ?")

YEAR = 2022

try:
    birthyear = int(s)
    print(f"En 2022, tu auras {YEAR - birthyear} ans.")

except TypeError:
    print ("Toi, t'as été berçé trop près du mur. On te demande une année ! En chiffres!")
