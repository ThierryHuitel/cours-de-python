""" Je suis une chaîne
 de caractères multilignes"""

MONTEXTE = "je suis une chaîne de caractères avec l'apostrophe qui passe sans problème."

# retour à la ligne
print ("a\nb")

# codes unicode
# coeur
print ("\u2764")

# happy
print ("\u263B")

# sad
print ("\u2639")

# tete de mort
print ("\u2620")

# La chaine de caractère sera interprétée de façon brute.
print(r"c:\dropbox\test")
