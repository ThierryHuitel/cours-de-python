"""
module
Implementation of rock, paper, scissors by Kylie Ying
YouTube Kylie Ying: https://www.youtube.com/ycubed
Twitch KylieYing: https://www.twitch.tv/kylieying
Twitter @kylieyying: https://twitter.com/kylieyying
Instagram @kylieyying: https://www.instagram.com/kylieyying/
Website: https://www.kylieying.com
Github: https://www.github.com/kying18
Programmer Beast Mode Spotify playlist:
https://open.spotify.com/playlist/4Akns5EUb3gzmlXIdsJkPs?si=qGc4ubKRRYmPHAJAIrCxVQ
"""

import random

def play():
    """ fonction qui est un exemple simple de jeu rock pamier ciseau"""
    humain = input("Quel est ton choix? 'r' pour rock, 'p' pour papier, 'c' pour ciseaux\n")
    ordi = random.choice(["r", "p", "c"])


    if humain == ordi:
        return "Egalité"
    elif (humain == "r" and ordi == "c"):
        return "Tu as gagné"
    elif (humain == "c" and ordi == "p"):
        return "Tu as gagné"
    elif (humain == "p" and ordi == "r"):
        return "Tu as gagné"
    else:
        return "Tu as perdu"
for i in range (5):
    print(play())
    