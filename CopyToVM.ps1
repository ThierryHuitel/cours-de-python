param(
    [Parameter(Mandatory=$true)]
    [string] $FILENAME,
    [switch] $service=$false
)

# Retourne $true si le fichier en paramètre existe sur la machine distante dans le répertoire /home/freebox, false sinon
function Test-FileExistsOnVm {
    param(
        [Parameter(Mandatory=$true)]
        [ValidateNotNull()]
        [string] $FILENAME
    )

    $(ssh -i ${SSH_KEY} freebox@${IP} "ls -l /home/freebox/${FILENAME}" 2>&1)  | out-null
    return $?
}

$SSH_KEY="C:\Users\$env:UserName\.ssh\id_ed25519"
$IP="192.168.0.27"

Write-Output "Fichier a envoyer: ${FILENAME}"
# Vérification de l'existance du fichier à envoyer sur la machine
$FILE = Get-ChildItem ${FILENAME} 2> $null
if ([string]::IsNullOrEmpty($FILE.FullName)) {
    Write-Error "Le fichier ${FILENAME} n'existe pas."
    exit 1
}

# Vérification de l'existance d'un fichier avec le même nom sur la VM distante
if (Test-FileExistsOnVm $FILE.Name) {
    # Si existance, suppression
    Write-Output "Fichier ${FILE} existant sur la VM."
    Write-Output "Suppression du fichier $($FILE.Name) sur la VM"
    ssh -i ${SSH_KEY} freebox@${IP} "rm -f /home/freebox/$($FILE.Name)"
}

# Envoi du fichier
Write-Output "Envoi du fichier $($FILE.FullName) vers la VM"
scp -i ${SSH_KEY} "$($FILE.FullName)" freebox@${IP}:/home/freebox/$($FILE.Name)

# Début processus de création du service seulement si paramètre -service passé à CopyToVM.ps1
if ($service) {
    Write-Output "Parametre -service detecte, debut processus de creation de service"
    # Suppression de service existant
    if (Test-FileExistsOnVm "delete_service.sh") {
        Write-Output "Script /home/freebox/delete_service.sh existant, suppression du service $($FILE.BaseName)"
        ssh -i ${SSH_KEY} freebox@$IP "/home/freebox/delete_service.sh $($FILE.BaseName)"
    } else {
        Write-Error "Fichier delete_service.sh absent sur la VM, l'envoyer sur la VM avant le fichier $($FILE.Name) sur la VM"
        exit 1
    }

    # Création du service
    if (Test-FileExistsOnVm "create_service.sh") {
        # Si le fichier en paramètre est bien un fichier .py, création de service
        if("$($FILE.Extension)" -eq ".py") {
            Write-Output "Creation du service sur la VM a partir du script $($FILE.Name)"
            ssh -i $SSH_KEY freebox@$IP "/home/freebox/create_service.sh $($FILE.Name)"
        } else {
            Write-Error "Fichier invalide pour creation de service (creation de service seulement avec ficher .py): $($FILE.Name)"
            exit 1
        }
    } else {
        Write-Output "Script create_service.sh absent sur la VM, le service n'est pas cree a partir du fichier envoye"
        exit 1
    }
}
