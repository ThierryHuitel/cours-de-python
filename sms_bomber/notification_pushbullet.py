
# pip install asyncpushbullet
import asyncpushbullet #pour envoi des notifications pushbullet

## Activer les deux lignes ci-dessous et envoyer les 3 en interactif pour avoir la liste des téléphones inscrits dans pushbullet de ce PC
pb = asyncpushbullet.AsyncPushbullet('o.UGJWKsDKSTnYYJMvW2toHSHtkrEj6NC3')
print(pb.devices)

def envoi_notification_pushbullet(titre, message):
    """
    exemples d'utilisation
            # envoi d'une notification pushbullet
            print(notification_pushbullet.envoi_notification_pushbullet(maChaine, message_a_envoyer))
    """
    # jeton pushbullet nécessaire pour utiliser l'API    
    pb = asyncpushbullet.AsyncPushbullet('o.UGJWKsDKSTnYYJMvW2toHSHtkrEj6NC3')
    #envoyer print(pb.devices) pour obtenir la liste des appareils. Choisir celui ou ceux qui vont recevoir les notitications  et les inscrire en dur dans le code ci-dessous
    
    try:
        ## définit le destinataire de la notification
        #ma_destination = pb.get_device('MAFRISIUS')
        ## envoie la notification
        #push = pb.push_note(titre, message, device=ma_destination)
        
        # définit le destinataire de la notification
        ma_destination = pb.get_device('Galaxy Note 10')
        # envoie la notification        
        push = pb.push_note(titre, message, device=ma_destination)
        return "Envoi de la notification: OK"
    except:
        return "Envoi de la notification: ECHEC"

def envoi_sms_pushbullet(message, str_numero_du_destinataire):
    """
    exemples d'utilisation
            ## envoi d'un sms
            #print(notification_pushbullet.envoi_sms_pushbullet(message_a_envoyer, "+33749439717"))
    """
    # jeton pushbullet nécessaire pour utiliser l'API  
    pb = asyncpushbullet.AsyncPushbullet('o.UGJWKsDKSTnYYJMvW2toHSHtkrEj6NC3')
    #envoyer print(pb.devices) pour obtenir la liste des appareils. Choisir celui ou ceux qui vont recevoir les notitications  et les inscrire en dur dans le code ci-dessous
    try:
        # définit l'appareil qui va envoyer le SMS
        mon_origine = pb.get_device('Galaxy Note 10')
        # envoie le SMS
        push = pb.push_sms(mon_origine, str_numero_du_destinataire, message)
        return "Envoi du SMS: OK"
    except:
        return "Envoi du SMS: ECHEC"