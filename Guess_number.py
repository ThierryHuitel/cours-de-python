""" exemple simple de jeu devine un nombre """
import random

def guess():
    """ devine un nombre avec while et if """
    nombre_aleatoire = random.randint(1, 10)
    nombre_que_je_devine = 0
    while nombre_que_je_devine != nombre_aleatoire:
        nombre_que_je_devine = int(input("devine un nombre entre 1 et 10: "))
        if nombre_que_je_devine < nombre_aleatoire:
            print("Trop bas.")
        elif nombre_que_je_devine > nombre_aleatoire:
            print("Trop heut.")
    print(f"bravo, tu as deviné ! C'était bien {nombre_aleatoire}")

guess()

# exercice possible: l'ordinateur devine https://www.youtube.com/watch?v=8ext9G7xspg&t=1274s
# ajouter nombre de fois
