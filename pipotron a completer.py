""" Ce module concentre les éléments des différents pipotrons thématiques.
Un pipotron est un programme qui fabrique des phrases pipeau tellement vides de sens
qu'elles fonctionnent même avec des éléments choisis aléatoirement.
"""
import random

def pipotron_scolaire():
    """Cette fonction va choisir aléatoirement un élément dans chaque section
    et les assembler en une phrase

    Paramètres:
    -----------
    Aucun

    Renvoie:
    --------
    Un texte au format Str
    """

    # Modifier cette liste avec tous les prénoms masculins de votre classe.
    section_1 = ["Pierre-François",
                "Fred",
                "Alban",
                "Kévin",
                "Jordan",
                "Johnny",
                "Maël",
                "Yves"]

    # pas d'expression finissant par 'de' dans cette section,
    # car les prénoms de la section suivante peuvent commencer par des voyelles
    section_2 = ["a plaqué",
                "s'est pris un râteau avec",
                "s'est tapé",
                "est sorti avec",
                "a collé une baffe à",
                "a dormi avec",
                "a roulé une pelle à",
                "a peloté",
                "s'est isolé dans les toilettes avec",
                "a été vu samedi avec",
                "a écrit une chanson à",
                "était à la soirée de",
                "est secrètement amoureux de",
                "a envoyé un petit mot à",
                "a fait un bisou à",
                "a envoyé des SMS très chauds à",
                "a joué à la bouteille avec"]

    # à modifier avec les noms des filles de la classe
    section_3 = ["Solange",
                "Maud",
                "Julie",
                "Jade",
                "Sophie",
                "Miranda",
                "Hélène"]

    section_4 = ["Du coup",
                "Alors forcément",
                "Donc bien sûr",
                "Il fallait s'y attendre",
                "Comme on peut se l'imaginer",
                "Evidemment"]

    section_5 = section_3

    section_6 = ["n'était pas au courant!",
                "lui fait un tout petit peu la gueule depuis.",
                "lui a rendu la monnaie de sa pièce!",
                "aurait voulu remettre ça avec lui!",
                "n'arrête pas de le coller.",
                "lui a demandé quelques explications!",
                "a trouvé ça très moyen.",
                "s'en est aperçue..."]

    section_7 = ["Elle en a parlé avec",
                "Elle aurait eu un accident avec",
                "Elle n'a rien dit à",
                "Elle a essayé de la brancher sur",
                "Elle a téléphoné à",
                "Elle a tout raconté à",
                "Elle a bavé sur",
                "Elle est allée à la soirée chez"]

    section_8 = section_1

    section_9 = ["et elle l'a traité de",
                "et elle a raconté à tout le monde que c'est un",
                "en oubliant de préciser qui, dans l'histoire, était le plus",
                "qui vraiment a réagi comme un",
                "qui, aux yeux de tout le monde, s'est comporté comme un",
                "qui a maintenant une réputation de",
                "qui s'est conduit comme un"]

    # choisir ici des insultes commençant par une consonne
    # (à cause des 'de' de la section précédente)
    section_10 = ["salaud.",
                 "connard.",
                 "chieur.",
                 "mysogine.",
                 "gamin.",
                 "pervers.",
                 "comique.",
                 "bâtard.",
                 "fdp.",
                 "nase."]

    mon_pipotron_scolaire = f"{random.choice(section_1)} {random.choice(section_2)} {random.choice(section_3)}. {random.choice(section_4)}, {random.choice(section_5)} {random.choice(section_6)} {random.choice(section_7)} {random.choice(section_8)} {random.choice(section_9)} {random.choice(section_10)}"

    return mon_pipotron_scolaire

print(pipotron_scolaire())
