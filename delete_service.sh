#!/bin/bash

Help()
{
   echo "Script de suppression d'un service à partir de son nom."
   echo
   echo "Syntax: ./delete_service <nom_de_service>"
   echo
   echo "    Avec nom_de_service = nom du service à supprimer"
   echo
   echo "Exemple: ./delete_service.sh bot2"
}

service_exists() {
    local n=$1
    if [[ $(sudo systemctl list-units --all -t service --full --no-legend "$n.service" | sed 's/^\s*//g' | cut -f1 -d' ') == $n.service ]]; then
        return 0
    else
        return 1
    fi
}

if [ "$#" -ne 1 ]; then
    Help
    echo "Paramètre nom de service manquant"
    exit 1
fi

service_name="$1"
# Garde fou pour éviter de manipuler un nom de service terminant par ".service" => on ne veut que le nom avant cette extension
service_name=${service_name%.service}

is_service_installed=$(sudo systemctl list-units -all | grep "Script python de titi" | awk '{print $1}' | grep $service_name)
if [ "" = $is_service_installed ]
then
    echo "Le service $service_name n'est pas un service créé par titi, impossible de le supprimer"
fi

if service_exists $service_name; then
    sudo systemctl stop $service_name
    sudo systemctl disable $service_name
    sudo find -L /etc/systemd/system -samefile /etc/systemd/system/$service_name.service -delete
    sudo rm /etc/systemd/system/$service_name.service
    sudo systemctl daemon-reload
    sudo systemctl reset-failed
else
    echo "Le service $service_name n'existe pas"
fi
