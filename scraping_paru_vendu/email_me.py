""" Ce module sert à envoyer des emails. Usage: 
dans le haut du module: 
import: email_me

dans le code, pour envoyer l'email, mettre dans l'ordre les arguments suivants:
nom_de_l_objet, corps_du_texte, nom_du_fichier_joint

exemple:
email_me.send_email(maChaine, Mon_speech + " " + str_mon_tableau_statistique + "\n Voici les éventuelles bonnes affaires:" + ma_liste_des_bonnes_affaires, maChaine + ".jpg")
"""

# Import smtplib for the actual sending function
import smtplib

# And imghdr to find the types of our images
import imghdr

def send_email(nom_de_l_objet, corps_du_texte, nom_du_fichier_joint):

    # Here are the email package modules we'll need
    from email.message import EmailMessage

    # Create the container email message.
    msg = EmailMessage()
    msg['Subject'] = nom_de_l_objet
    me = "kgb@huitel.net"
    family = "thierry@huitel.net"
    msg['From'] = me
    #msg['To'] = ', '.join(family)
    msg['To'] = family
    msg.preamble = 'Bonne affaire'
    msg.set_content(corps_du_texte)
    username = me # votre login ici
    password = "tagem40p!!!"

    # Open the files in binary mode.  Use imghdr to figure out the
    # MIME subtype for each specific image.

    if nom_du_fichier_joint != "":
        with open(nom_du_fichier_joint, 'rb') as fp:
            img_data = fp.read()
        msg.add_attachment(img_data, maintype='image',
                                        subtype=imghdr.what(None, img_data))

    # Send the email via our own SMTP server.
    with smtplib.SMTP('mail.huitel.net:587') as s:
        s.starttls()
        s.login(username,password)
        s.send_message(msg)
        s.close()
        print("Message envoyé !")