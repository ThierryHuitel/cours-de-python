#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

This example shows an icon
in the titlebar of the window.

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""



import sys
from PyQt5.QtGui import (QIcon, QFont)
from PyQt5.QtWidgets import (QMainWindow, QWidget, QToolTip, 
    QPushButton, QApplication, QMessageBox)
from PyQt5.QtCore import QCoreApplication


class Example(QMainWindow):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
        
    def initUI(self):
        #statusbar
        self.statusBar().showMessage('Ready')
        #taille de la fenêtre
        self.setGeometry(300, 300, 300, 220)
        #Icone
        self.setWindowIcon(QIcon('H violet.ico'))        
        #tooltip
        QToolTip.setFont(QFont('SansSerif', 10))
        self.setToolTip('This is a <b>QWidget</b> widget')
        #bouton
        btn = QPushButton('Button', self)
        btn.setToolTip('This is a <b>QPushButton</b> widget')
        btn.resize(btn.sizeHint())
        btn.move(10, 10)       
        #Fermer la fenetre
        qbtn = QPushButton('Fermer', self)
        qbtn.clicked.connect(QCoreApplication.instance().quit)
        qbtn.resize(qbtn.sizeHint())
        qbtn.move(10, 30)    

        self.setWindowTitle('Tooltips')    
        self.show()

        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())