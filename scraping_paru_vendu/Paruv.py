import csv                           # pour écrire dans un fichier csv
import sys                           #pour décoder les fichiers entrants
import string
import webbrowser                    # pour ouvrir directement dans le navigateur les pages des bonnes affaires !
import codecs                        #pour encoder et décoder les fichiers (et les avoir avec les accents corrects)
import datetime                      # envoi email tous les jours pour dire que tout marche bien, même s'il n'y a pas d'annonces.
import time                         # pour chronométrer chaque recherche
import email_me
import requests
import liste_des_recherches
import notification_pushbullet
import numpy as np
import matplotlib.pyplot as plt         # pour graphique
import matplotlib.lines as lines
from bs4 import BeautifulSoup
#from pygame import mixer                # Load the required library to play mp3 sound
MODE_BAVARD =0
date = datetime.datetime.now()
synthese_du_controle_des_durees = " Durée des recherches de plus de 5 secondes: \n"
chrono_debut_du_programme = time.clock()
synthese_chrono_total = "\n Durée totale du programme: "
def nettoie_cette_liste_de_string_de_toute_ponctuation(ma_liste_de_string):
    """nécessite 'import string'
    prend une liste avec des mots qui sont suivis de ponctuation, cela enlève les ponctuations et renvoie la liste nettoyée.
    """
    ma_liste_de_string_nettoyee_de_toute_ponctuation = [''.join(c for c in s if c not in string.punctuation) for s in ma_liste_de_string]
    return ma_liste_de_string_nettoyee_de_toute_ponctuation

def transforme_un_texte_au_format_string_en_liste_de_mots(mon_texte_au_format_string):
    """on rentre une phrase, et cela produit une liste de mots cela coupe à chaque espace.  Il faudra ensuite probablement enlever les ponctuations (appeler nettoie_cette_liste_de_string_de_toute_ponctuation) et puis ça met tout en minuscules
    """
    return mon_texte_au_format_string.split(" ")

def joins_ces_deux_listes_en_une_seule(liste1, liste2):
    """ on lui donne deux listes en argument, et ça les rassemble en une seule"""
    return (liste1 + liste2)

def verifie_les_imperatifs_dans_le_titre(str_titre_de_mon_annonce, mots_obligatoires_dans_le_titre):
    """ Recherche des mots obligatoires
    Dans les mots obligatoires que je décide, il faut diviser au maximum les éléments. exemple si je cherche un lumix fx3000, si je mets une recherche pour "lumix", "fx", "3000", la recherche sera bonne, même si c'est écrit sous la forme lumix fx-3000, ou encore lumix fx.3000 puisque l'on trouvera ces petits bouts de chaine dans toutes les expressions.
    -----------------------------------------
    mots_contenus_dans_le_titre_de_mon_annonce = "sansun galaxy s4"
    mots_obligatoires_dans_le_titre = [["samsung", "samsun", "sansun", "sansung", "sam sung"], ["galaxy", "galaksi"], ["s4"]]
    on part du principe que c'est bon à la base.  Si un des blocs reste à zéro, cela fera une multiplication par zéro, et donc un résultat à zéro.
    """
    faut_il_garder_cette_annonce = 1 #oui
    la_recherche_dans_l_ensemble_est_elle_concluante = 1
    # Pour chacun des blocs de mots impératifs
    ##print("Titre d'annonce étudié: " + str_titre_de_mon_annonce)
    for mon_bloc in mots_obligatoires_dans_le_titre:        
        la_recherche_dans_le_bloc_est_elle_concluante = 0
        faut_il_continuer_la_recherche_dans_ce_bloc = 1

        # Pour chacun des mots contenus dans chaque bloc
        for mon_string in mon_bloc:
            #print("on cherche ce mot obligatoire: " + mon_string)
            if faut_il_continuer_la_recherche_dans_ce_bloc:
                # Je le compare à chacun des mots du titre de l'annonce. je veux savoir si chaque bloc de mots se trouve dans mon_mot_du_titre si oui, le mot_trouve = 1 donc je cherche à savoir si le premier OU le deuxieme ou le troisième...  correspondent donc si l'addition de tous les éléments est supérieur à 1 -1 veut dire pas trouvé docn si la recherche donne autre chose que -1, la recherche est concluante. si la recherche est concluante: 1, si pas trouvé: 0print ("Je cherche: " + mon_string.lower()+ " dans le titre:"+ str_titre_de_mon_annonce)
                if str_titre_de_mon_annonce.find(mon_string.lower()) != -1 or str_titre_de_mon_annonce.find(mon_string.title()) != -1 or str_titre_de_mon_annonce.find(mon_string.upper()) != -1:
                    #print("trouvé")
                    la_recherche_dans_le_bloc_est_elle_concluante = 1
                    faut_il_continuer_la_recherche_dans_ce_bloc = 0
                    break

            else:
                #si on a déjà trouvé une correspondance dans le bloc, pas lapeine de tester les autres orthographes, passons au blocsuivant.print("correspondance trouvée dans le bloc, on passe au bloc suivant")
                break

        la_recherche_dans_l_ensemble_est_elle_concluante = la_recherche_dans_l_ensemble_est_elle_concluante * la_recherche_dans_le_bloc_est_elle_concluante
        #print("recherche dans l'ensemble:" + str(la_recherche_dans_l_ensemble_est_elle_concluante))
    if la_recherche_dans_l_ensemble_est_elle_concluante == 0:
        faut_il_garder_cette_annonce = 0 #non
        if MODE_BAVARD == 1:
            print("Eliminé car pas trouvé les mots obligatoires dans le titre: " + str_titre_de_mon_annonce)
    else:
        if MODE_BAVARD == 1:
            print("l'annonce est à ce stade retenue")
        faut_il_garder_cette_annonce = 1 #oui
    return faut_il_garder_cette_annonce

def verifie_les_imperatifs_dans_le_titre_et_la_description(str_titre_de_mon_annonce, ma_liste_des_orthographes_divergents):
    """********************************************************************************************************
     C'est une recherche de deuxième rideau, pour les articles un peu flous, qui, contrairement au galaxy S5 n'ont pas forcément leur caractéristique dans le titre (ex: portail de 4m de large, ou cocotte de 35cm) elle ne se déclenche que s'il y a quelque chose dans la liste de mots obligatoires pour le descriptif
    ********************************************************************************************************
     mots_contenus_dans_le_titre_de_mon_annonce = "sansun galaxy s4"
     ma_liste_des_orthographes_divergents = [["samsung", "samsun", "sansun", "sansung", "sam sung"], ["galaxy", "galaksi"], ["s4"]]
     on part du principe que c'est bon à la base.  Si un des blocs reste à zéro, cela fera une multiplication par zéro, et donc un résultat à zéro.
    """
    faut_il_garder_cette_annonce = 1 #oui
    la_recherche_dans_l_ensemble_est_elle_concluante = 1
    # Pour chacun des blocs de mots impératifs
    for mon_bloc in ma_liste_des_orthographes_divergents:
        la_recherche_dans_le_bloc_est_elle_concluante = 0
        faut_il_continuer_la_recherche_dans_ce_bloc = 1

        # Pour chacun des mots contenus dans chaque bloc
        for mon_string in mon_bloc:
            if faut_il_continuer_la_recherche_dans_ce_bloc:
                # Je le compare à chacun des mots du titre de l'annonce.
                # je veux savoir si chaque bloc de mots se trouve dans
                # mon_mot_du_titre
                # si oui, le mot_trouve = 1
                # donc je cherche à savoir si le premier OU le deuxieme ou le
                # troisième...  correspondent
                # donc si l'addition de tous les éléments est supérieur à 1

                # -1 veut dire pas trouvé
                # docn si la recherche donne autre chose que -1, la
                # recherche est concluante.
                # si la recherche est concluante: 1, si pas trouvé: 0
                if str_titre_de_mon_annonce.find(mon_string.lower()) != -1 or str_titre_de_mon_annonce.find(mon_string.title()) != -1 or str_titre_de_mon_annonce.find(mon_string.upper()) != -1:
                    la_recherche_dans_le_bloc_est_elle_concluante = 1
                    faut_il_continuer_la_recherche_dans_ce_bloc = 0
                    break
            else:
                #si on a déjà trouvé une correspondance dans le bloc, pas la
                #peine de tester les autres orthographes, passons au bloc
                #suivant.
                break

        la_recherche_dans_l_ensemble_est_elle_concluante = la_recherche_dans_l_ensemble_est_elle_concluante * la_recherche_dans_le_bloc_est_elle_concluante
        #print("recherche dans l'ensemble:" +
        #str(la_recherche_dans_l_ensemble_est_elle_concluante))
    if la_recherche_dans_l_ensemble_est_elle_concluante == 0:
        faut_il_garder_cette_annonce = 0 #non
        if MODE_BAVARD == 1:
            print("Eliminé car pas trouvé les mots obligatoires dans la description: " + str_titre_de_mon_annonce)
    else:
        faut_il_garder_cette_annonce = 1 #oui
    return faut_il_garder_cette_annonce

def verifie_les_mots_interdits_dans_le_titre(str_titre_de_l_annonce_a_verifier):
    """**************************************************************************************************
     Cette fonction récupère le titre de l'annonce et renvoie si oui (1) ou non (0) il faut l'ajouter à l'array. après vérification des mots interdits
    **************************************************************************************************
    """
    #------------------------------------------------------------------
    # Gestion des mots à bannir
    faut_il_garder_cette_annonce = 1 #oui
    liste_complete_des_mots_a_bannir_du_titre = [ ]

    #j'ajoute à la liste complete la liste initiale, la même avec la première
    #lettre en majuscule, puis tout en capitales.
    for elements in  mots_interdits_dans_le_titre[:]:
        liste_complete_des_mots_a_bannir_du_titre.append(elements)
        liste_complete_des_mots_a_bannir_du_titre.append(elements.title())
        liste_complete_des_mots_a_bannir_du_titre.append(elements.upper())

    #print("début de la boucle pour effacer les annonces merdiques")
    for element in liste_complete_des_mots_a_bannir_du_titre:
        if str_titre_de_l_annonce_a_verifier.find(element) != -1:
            faut_il_garder_cette_annonce = 0
            if MODE_BAVARD == 1:
                print("Eliminé car un mot interdit: " + str(element) + " a été trouvé dans ce titre:" + str_titre_de_l_annonce_a_verifier)

    return faut_il_garder_cette_annonce

def verifie_les_mots_interdits_dans_le_descriptif(str_mon_descriptif):
    """**************************************************************************************************
     Cette fonction récupère le titre de l'annonce et renvoie si oui (1) ou non (0) il faut l'ajouter à l'array. après vérification des mots interdits
    **************************************************************************************************"""
    #------------------------------------------------------------------
    # Gestion des mots à bannir
    faut_il_garder_cette_annonce = 1 #oui
    liste_complete_des_mots_a_bannir_du_descriptif = [ ]

    #j'ajoute à la liste complete la liste initiale, la même avec la première
    #lettre en majuscule, puis tout en capitales.
    for elements in  mots_non_souhaites_dans_le_descriptif[:]:
        liste_complete_des_mots_a_bannir_du_descriptif.append(elements)
        liste_complete_des_mots_a_bannir_du_descriptif.append(elements.title())
        liste_complete_des_mots_a_bannir_du_descriptif.append(elements.upper())

    #print("début de la boucle pour effacer les annonces merdiques")
    for element in liste_complete_des_mots_a_bannir_du_descriptif:
        if str_mon_descriptif.find(element) != -1:
            faut_il_garder_cette_annonce = 0
            if MODE_BAVARD == 1:
                print("Eliminé car un mot interdit: " + str(element) + " a été trouvé dans ce descriptif:" + str_mon_descriptif)

    return faut_il_garder_cette_annonce

def verifie_la_coherence_du_prix(prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai, prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf, prix_de_l_annonce_a_verifier):
    """ Cette fonction vérifie si le prix de l'annonce est bien dans les limites fixées dans les paramètres de la recherche"""
    faut_il_garder_cette_annonce = 1 #oui
    #------------------------------------------------------------------
    # Effacer les annonces dont le prix est ridicule (inférieur à la limite fixée dans les paramètres pour l'article)
    #**************************************************************************************************
    effacer_direct_les_annonce_dont_le_prix_est_inferieur_a = int(prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai)
    if prix_de_l_annonce_a_verifier < effacer_direct_les_annonce_dont_le_prix_est_inferieur_a:
        faut_il_garder_cette_annonce = 0
        if MODE_BAVARD == 1:
            print("Eliminé car le prix est inférieur au prix minimal souhaité: " + str(prix_de_l_annonce_a_verifier))
    #------------------------------------------------------------------
    # Effacer les annonces dont le prix est trop élevé (plus cher que le neuf, ou rien à voir avec l'article) (supérier à la limite fixée dans les paramètres pour l'article)
    #**************************************************************************************************
    effacer_direct_les_annonce_dont_le_prix_est_superieur_a = int(prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf)
    if prix_de_l_annonce_a_verifier > effacer_direct_les_annonce_dont_le_prix_est_superieur_a:
        faut_il_garder_cette_annonce = 0
        if MODE_BAVARD == 1:
            print("Eliminé car le prix est supérieur au prix maximal fixé: " + str(prix_de_l_annonce_a_verifier))
    return faut_il_garder_cette_annonce
                
def controle(texte_a_verifier):
    """**************************************************************************************************
     utilisé dans la fabrication du lien http de la recherche
    **************************************************************************************************"""
    if texte_a_verifier.count(" ") != 0:
        return texte_a_verifier.index(" ")
    else:
        return len(texte_a_verifier)

def strip_tags(html_to_process, invalid_tags):
    """**************************************************************************************************
     Nettoyage d'un code HTML pour en enlever certains tags que l'on ne veut pas.
    **************************************************************************************************
    Remove from the given html code all the specified tags
    html_to_process: contains the html code to clean
    invalid_tags: liste des tags à enlever du code 
        example: 
        invalid_tags = ['b', 'i', 'u']
    """
    internal_soup = BeautifulSoup(html_to_process, 'html.parser')

    for tag in internal_soup.findAll(True):
        if tag.name in invalid_tags:
            s = ""

            for mon_contenu in tag.contents:
                if not isinstance(mon_contenu, NavigableString):
                    mon_contenu = strip_tags(str(mon_contenu), invalid_tags)
                s += str(mon_contenu)

            tag.replaceWith(s)
    return internal_soup

def clean_tags(dirty_tag):
    """**************************************************************************************************
     prend un tag, renvoie le texte du tag en question, nettoyé.
    *****************************************************************************************************"""
    # Remove unwanted spaces from the text of the tag.
    cleaned_tag_text = dirty_tag.text.replace('\xa0€\n', "")
    cleaned_tag_text = cleaned_tag_text.replace('+', "")
    cleaned_tag_text = cleaned_tag_text.replace('\xa0€', "")
    cleaned_tag_text = cleaned_tag_text.replace('\n', "")
    cleaned_tag_text = cleaned_tag_text.replace('\t', "")
    cleaned_tag_text = cleaned_tag_text.replace('<br\>', "")
    cleaned_tag_text = cleaned_tag_text.replace('/', " / ")
    cleaned_tag_text = cleaned_tag_text.replace(' ', "")
    cleaned_tag_text = cleaned_tag_text.replace('/', " / ")
    cleaned_tag_text = cleaned_tag_text.strip()
    return cleaned_tag_text  

def clean_price(dirty_string):
    """**************************************************************************************************
     prend un string, et le renvoie nettoyé.
    **************************************************************************************************"""
    # Remove unwanted spaces from the text of the tag.
    try:
        cleaned_string = dirty_string.replace('\xa0€\n', "")
        cleaned_string = cleaned_string.replace('\xa0€', "")
        cleaned_string = cleaned_string.replace(' €', "")
        cleaned_string = cleaned_string.replace(',', "")
        cleaned_string = cleaned_string.replace('"', '')
        cleaned_string = cleaned_string.replace('-', '')
        cleaned_string = cleaned_string.replace('\r\n', ",")
        cleaned_string = cleaned_string.replace('\r', "")
        cleaned_string = cleaned_string.replace('\n', "")
        cleaned_string = cleaned_string.replace('+', "")
        cleaned_string = cleaned_string.replace('\t', "")
        cleaned_string = cleaned_string.replace('/', " / ")
        cleaned_string = cleaned_string.replace('<br\>', "")
        cleaned_string = cleaned_string.replace('/', " / ")
        cleaned_string = cleaned_string.replace(' ', "")
        cleaned_string = cleaned_string.replace(',', "")
        for _ in range(200):
            cleaned_string = cleaned_string.replace('  ', ' ')
        cleaned_string = cleaned_string.strip()
    except:
        #print("erreur, un élément nul a été envoyé à la fonction clean_string"
        #+ str(i) + str(clean_article_text))
        cleaned_string = "0"
    return cleaned_string  

def clean_string(dirty_string):
    """**************************************************************************************************
     prend un string, et le renvoie nettoyé.
    **************************************************************************************************"""
    # Remove unwanted spaces from the text of the tag.
    try:
        cleaned_string = dirty_string.replace('\xa0€\n', "")
        cleaned_string = cleaned_string.replace('\xa0€', "")
        cleaned_string = cleaned_string.replace(' €', "")
        cleaned_string = cleaned_string.replace(',', "")
        cleaned_string = cleaned_string.replace('"', '')
        cleaned_string = cleaned_string.replace('-', '')
        cleaned_string = cleaned_string.replace('\r\n', ",")
        cleaned_string = cleaned_string.replace('\r', "")
        cleaned_string = cleaned_string.replace('\n', "")
        cleaned_string = cleaned_string.replace('+', "")
        cleaned_string = cleaned_string.replace('\t', "")
        cleaned_string = cleaned_string.replace('/', " / ")
        cleaned_string = cleaned_string.replace('<br\>', "")
        cleaned_string = cleaned_string.replace('/', " / ")
        for _ in range(200):
            cleaned_string = cleaned_string.replace('  ', ' ')
        cleaned_string = cleaned_string.strip()
    except:
        #print("erreur, un élément nul a été envoyé à la fonction clean_string"
        #+ str(i) + str(clean_article_text))
        cleaned_string = "0"
    return cleaned_string      

def cree_la_fin_de_l_url_a_partir_des_mots_de_la_recherche(local_str_ma_recherche):
    """**************************************************************************************************
     Creation de la fin de l'url à rechercher à partir des mots de la recherche
    **************************************************************************************************"""
    compte_du_nombre_des_espaces = local_str_ma_recherche.count(" ")
    debut = 0
    fin = controle(local_str_ma_recherche)
    maListe = [ ]
 
    for i in range(0, compte_du_nombre_des_espaces + 1):
        maListe.append(local_str_ma_recherche[debut:fin])
        local_str_ma_recherche = local_str_ma_recherche[fin + 1:]
        fin = controle(local_str_ma_recherche)
    local_ma_recherche_mise_en_forme_pour_l_adresse_https = ""

    for j, mon_element_de_liste in enumerate(maListe):
        local_ma_recherche_mise_en_forme_pour_l_adresse_https = local_ma_recherche_mise_en_forme_pour_l_adresse_https + mon_element_de_liste + "+"
    # Supprimer le dernier plus qui n'a rien à faire là.
    local_ma_recherche_mise_en_forme_pour_l_adresse_https = local_ma_recherche_mise_en_forme_pour_l_adresse_https[:-1]
    return local_ma_recherche_mise_en_forme_pour_l_adresse_https

def ouvre_un_fichier_csv_le_decode_et_renvoie_son_contenu(mon_nom_de_fichier):
    """**************************************************************************************************
     Lecture des éléments déjà enregistrés dans le passé. Si la recherche a déjà été faite: un fichier à ce nom existe, et ça va l'ouvrir. Ca importe et décode le fichier utf8 dans un premier temps. On suppose ici que le fichier est dans le répertoire où se trouve le code. Donc pas besoin de donner son chemin, juste le nom du fichier. La fonction renvoie soit le texte du fichier (string), soit une chaine vide
    **************************************************************************************************
    """
    try:
        fichier_a_ouvrir = mon_nom_de_fichier.lower()
        # tente d'ouvrir le fameux fichier, en extrait le texte et le décode. utf8 car c'est le must.
        with codecs.open(fichier_a_ouvrir,'r', encoding='utf8') as mon_fichier:
            text_issu_du_fichier = mon_fichier.read()
        mon_fichier.close()
        # Je remplace les retour à la ligne par des virgules, sinon ça fout la merde lors de la transformation en numpy array
        text_issu_du_fichier = text_issu_du_fichier.replace('\r\n', ",")
        text_issu_du_fichier = text_issu_du_fichier.replace('+', "")
        return text_issu_du_fichier
    except:
        if MODE_BAVARD == 1:
            print("le fichier " + mon_nom_de_fichier + " n'a pas été trouvé")
        return ""

def importe_le_fichier_d_archive_s_il_y_en_a_un(text_from_file):
    """
        S'il y avait des données antérieures, cette fonction va mettre toutes dans un numpy array identique à mon_array_au_propre de la fin du programme.
    """
    if text_from_file != "":
        try:
            # Ca cree un numpy array en une dimension: chaque virgule crèe un
            # nouvel élément.
            mon_array_temporaire_a_une_dimension = np.array(text_from_file.split(','))
            # Calcule combien il y a d'annonces, pour savoir comment créer l'array
            # en 2D
            nombre_d_annonces = int(mon_array_temporaire_a_une_dimension.size / nombre_d_informations_par_annonce)
            # Création de l'array définitif, bien dimensionné.
            nparray_importe = np.empty((nombre_d_annonces, nombre_d_informations_par_annonce), dtype=object)
            # Je place chaque élément de l'array 1D à sa place dans l'array 2D
            mon_i = 0
            mon_j = 0
            for element in mon_array_temporaire_a_une_dimension:
                if element != "":
                    #combien_d_elements_dans_mon_array2 +=1
                    nparray_importe[mon_i, mon_j] = element
                    if mon_j == 6:
                        mon_j = 0
                        mon_i += 1
                    else:
                        mon_j += 1
                else:
                    # dans le cas où il y a un espace qui traine à la fin du
                    # fichier...
                    break
            # Je supprime les variables non utilisés.
            del mon_array_temporaire_a_une_dimension
            text_issu_du_fichier = None
            print("Fichier importé. Etude rapide de: " + str(maChaine) + " en cours.")
    
        except:
            nparray_importe = [ ]
            # si le fichier n'existe pas, parce que la recherche n'a jamais été
            # faite.
    else:
        nparray_importe = [ ]
        print("Première recherche de: " + str(maChaine) + ". Un scan est lancé pour étudier chacune des offres au niveau national, et cela peut prendre un peu de temps.")
    return nparray_importe

def scanne_archive_analyse_et_graphique(str_ma_recherche,  prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai,  prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf,  mots_obligatoires_dans_le_titre,  mots_a_trouver_obligatoirement_dans_la_description_ou_le_titre, url_fourni):
    """**************************************************************************************************
     Creation de l'array qui va contenir les donnnées.
    **************************************************************************************************
    """
    if MODE_BAVARD == 1:
        print("\nRecherche: " + str(str_ma_recherche))
    #Cree un array numpy avec autant de lignes que d'articles, et 7 colonnes
    my_big_np_array_to_store_and_analyse_results = np.empty((taille_du_np_array, nombre_d_informations_par_annonce), dtype=object)
    suivi_de_la_ligne_a_remplir = 0 # initie la variable qui va contenir l'indexation de la ligne à remplir dans l'array, page après page.

    #Le nom du fichier est créé à partir des mots de la recherche et de l'extension csv.
    text_from_file = ouvre_un_fichier_csv_le_decode_et_renvoie_son_contenu(maChaine + ".csv") #maChaine est une variable issue de la boucle "for" initiale.
    
    #met les données issues du fichier (s'il y en a) dans un numpy array formaté aux bonnes dimensions.
    nparray_importe = importe_le_fichier_d_archive_s_il_y_en_a_un(text_from_file)
    
    # J'ai maintenant toutes mes archives dans un array nommé: nparray_importe

    # Création d'une liste contenant les numéros de tous les articles déjà dans
    # le fichier
    liste_des_numeros_d_articles_deja_dans_la_base = [ ]

    mon_i = 0
    for article in nparray_importe:
        if article.any() != "":
            # j'ajoute à ma liste tous les numéros d'articles qui sont déjà
            # dans le fichier.
            liste_des_numeros_d_articles_deja_dans_la_base.append(nparray_importe[mon_i, 5])
            mon_i += 1
        else:
            # dans le cas où il y a un espace qui traine à la fin du fichier...
            break
    
    # J'ai maintenant une liste des numéros des articles qui sont déjà dans le fichiers dans l'array: liste_des_numeros_d_articles_deja_dans_la_base

    #********************************************************************************************
    # Lancement de la boucle des pages
    #********************************************************************************************
    numero_de_la_page_du_bonc = 1
    faut_il_continuer = 1
    combien_d_elements_dans_mon_array = 0
    mon_prix_nettoye=0
    ma_recherche_mise_en_forme_pour_l_adresse_https = cree_la_fin_de_l_url_a_partir_des_mots_de_la_recherche(str_ma_recherche)
    #link_to_explore= url_fourni
    while faut_il_continuer == 1:
        # s'il n'y a pas de lien fourni avec la recherche, je vais créer moi-même le lien à partir des mots clés.
        #if url_fourni == "":
            #print("début du traitement de la page " +
            #str(numero_de_la_page_du_bonc))
        link_to_explore = 'https://www.paruvendu.fr/mondebarras/listefo/default/default/?fulltext='+ ma_recherche_mise_en_forme_pour_l_adresse_https +'&elargrayon=1&ray=50&idtag=&region=&r=&libelle_lo=Vannes+%2856000%29&codeinsee=56260&lo=56000&ray=50&zmd%5B%5D=VENTE&px0=&px1=&codPro=&filtre=&tri=&p='+ str(numero_de_la_page_du_bonc)
        ##sinon, si une url a été fournie, je vais l'adapter pour pouvoir changer son numéro de page automatiquement
        #else:
        #    # si le marqueur de page est un th, je je remplace par un o   
        #    if '?th=1' in url_fourni:
        #        link_to_explore = url_fourni.replace('?th=', '?o=')
        #    else:
        #        link_to_explore= url_fourni
        #    # l'url fourni a déjà subi une transformation, donc je modifie maintenant link_to_explore
        #    # s'il y a un marqueur de page dans le lien fourni, c'est OK
        #    if '?o=' in link_to_explore:
        #        pass
        #    # sinon s'il n'y a pas de marqueur o dans le lien, j'en rajoute un 
        #    else:
        #        link_to_explore = link_to_explore.replace('?', '?o=1&')
        #    # je me retrouve à ce stade avec un lien qui a obligatoirement un marqueur ?o=1
        #    # à chaque fois, c'est o=1 car les lignes au dessus réinitialisent le link_to_explore en lui affectant la valeur le url_fourni
        #    # j'enlève le numéro et je le remplace par la variable.
        #    link_to_explore = link_to_explore.replace('?o=1', '?o='+ str(numero_de_la_page_du_bonc))
        if MODE_BAVARD == 1:
            print(link_to_explore)
        #**************************************************************************************************
        # Récupération des données du site internet
        #**************************************************************************************************
        website_response = requests.get(link_to_explore)
        html_from_the_website = website_response.text
        #print(html) #prints all the html of the page !
        soup = BeautifulSoup(html_from_the_website, 'html.parser')

        #**************************************************************************************************
        # Nettoyage d'un fichier html pour en extraire le texte, et stockage
        # des données dans l'array numpy
        #**************************************************************************************************
        articles_title = (soup.find_all("li", {"class":"annonce"})) #renvoie tous les paragraphes h2, de la bonne classe du document.
        # Si on arrive à la fin des pages contenant des articles, il n'y aura pas de titre, et on saura qu'on peut arrêter.
        if articles_title == [ ]:
            faut_il_continuer = 0
            break
        else:
            invalid_tags = ['b', 'i', 'u', 'n', 't']
            compteur_pour_la_ligne_de_l_array = 0
            nombre_d_articles_de_cette_page_enregistres = 0
            # On passe en revue tous les articles de la page en question
            for art in articles_title:
                faut_il_enregistrer_cet_article = 1
                #------------------------------------------------------------------
                # Trouver le titre de l'article, et le passer à travers 3 filtres de nettoyage pour avoir un texte propre
                # Remove unwanted tags from html.
                dirty_articles_tags = art.find_next("h3")
                # Remove unwanted spaces from the text of the tag.
                clean_article_title = dirty_articles_tags.text.strip()
                clean_article_title = clean_string(clean_article_title)
                clean_article_title = clean_article_title.lower()
                # certains pros commencent leur titre par des tirets, et ça fait décaler les données dans le table donc je dégage les articles en question direct.
                if clean_article_title[:2] == "''":     # les 2 premiers caractères
                    faut_il_enregistrer_cet_article = 0
                #------------------------------------------------------------------
                if faut_il_enregistrer_cet_article == 1:
                    #trouver le prix grace au tag suivant de chaque article
                    mon_prix_enfin_trouve = art.find_next("p", {"class":"prix"})
                    try:
                        mon_prix_brut = mon_prix_enfin_trouve.get_text()
                        mon_prix_nettoye = clean_price(mon_prix_brut)

                        #J'ai trouvé le prix suivant: Je vérifie que je n'ai pas sauté à l'article suivant en vérifiant que j'ai le même prédécesseur.
                        le_tag_d_avant = mon_prix_enfin_trouve.find_previous('h3') # Je trouve le tag('a'), c'est-à-dire le lien hypertexte précédent.
                        les_attributs_du_tag_d_avant_le_prix = le_tag_d_avant.get_text() # Je récupère les attributs de ce lien hypertexte (car c'est un seul lien, on
                        if les_attributs_du_tag_d_avant_le_prix ==dirty_articles_tags:
                            print ("ok")
                        #                                                            # ne peut plus naviguer à l'intérieur...)
                        #le_lien_du_tag_juste_avant_le_prix = les_attributs_du_tag_d_avant_le_prix.get("href") #dans ces attributs, je choisis juste le véritable lien, l'adresse web.
                    except:
                        faut_il_enregistrer_cet_article = 0
                #------------------------------------------------------------------
                #trouver le lien internet grace au parent de chaque article
                if faut_il_enregistrer_cet_article == 1:
                    mes_parents = art.find_parent('a') # trouver les parents d'AT qui contiennent des liens hypertextes.
                    mes_attributs = mes_parents.attrs #sortir un dictionnaire des attributs de mes_parents
                    mon_lien = mes_attributs.get("href")
                    mon_lien = mon_lien.replace("//", "https://")

                #------------------------------------------------------------------
                # Explorer la page avec son descriptif
                if faut_il_enregistrer_cet_article == 1:
                    sous_link_to_explore = mon_lien
                    sous_website_response = requests.get(sous_link_to_explore)
                    sous_html_from_the_website = sous_website_response.text
                    sous_soup = BeautifulSoup(sous_html_from_the_website, 'html.parser')

                    mon_tag_de_descriptif = sous_soup.find("p", {"itemprop":"description"})
                    try:
                     mon_texte_de_descriptif = mon_tag_de_descriptif.contents
                    except:
                     mon_texte_de_descriptif = ""
                    mon_beau_texte_de_descriptif = ""
            
                    for tchatche in mon_texte_de_descriptif:
                     if isinstance(tchatche, str):
                         mon_beau_texte_de_descriptif = mon_beau_texte_de_descriptif + " " + tchatche.string
                     else:
                         pass
                    mon_descriptif = clean_string(mon_beau_texte_de_descriptif)
                    mon_descriptif = mon_descriptif.lower()

                #------------------------------------------------------------------
                #Trouver la catégorie
                if faut_il_enregistrer_cet_article == 1:
                    mon_paragraphe_suivant = art.find_next('p', {"itemprop":"category"})
                    mon_texte_de_paragraphe = mon_paragraphe_suivant.string
                    ma_categorie = clean_string(mon_texte_de_paragraphe)
                    if ma_categorie == '':
                        ma_categorie = "aucune"
                    ### Pas besoin de ci-dessous : il suffit de comparer avec mon_lien. J'ai trouvé la catégorie suivante: Je vérifie qu'elle a le même lien juste avant, pour m'assurer qu'un article sans prix n'a pas faussé les données.
                    le_tag_d_avant_la_categorie = mon_paragraphe_suivant.find_previous('a') # Je trouve le tag('a'), c'est-à-dire le lien hypertexte précédent.
                    les_attributs_du_tag_d_avant_la_categorie = le_tag_d_avant_la_categorie.attrs # Je récupère les attributs de ce lien hypertexte (car c'est un seul lien, on ne peut plus naviguer à l'intérieur...)
                    le_lien_du_tag_juste_avant_la_categorie = les_attributs_du_tag_d_avant_la_categorie.get("href") #dans ces attributs, je choisis juste le véritable lien, l'adresse web.

                #------------------------------------------------------------------
                #Trouver la ville
                if faut_il_enregistrer_cet_article == 1:
                    mon_paragraphe_suivant2 = art.find_next('p', {"itemprop":"availableAtOrFrom"})
                    ma_ville = mon_paragraphe_suivant2.get_text()
                    ma_ville = clean_string(ma_ville)

                #------------------------------------------------------------------
                #Trouver le numéro de l'annonce
                if faut_il_enregistrer_cet_article == 1:
                    mon_paragraphe_suivant3 = art.find_next("div", {"class":"saveAd"})
                    detail_de_l_annonce = ""
                    numero_de_l_annonce = ""
                    try:
                        detail_de_l_annonce = mon_paragraphe_suivant3.attrs #sortir un dictionnaire des attributs du prix
                        numero_de_l_annonce = detail_de_l_annonce.get("data-savead-id")
                    except:
                        numero_de_l_annonce = "00000"
                     #faut_il_enregistrer_cet_article = 0
                
                    # Confronter ce numéro à la liste des numéros déjà dans le fichier
                    for numero_existant in liste_des_numeros_d_articles_deja_dans_la_base:
                     # si le numéro de l'annonce existe déjà dans le fichier
                        if numero_de_l_annonce == numero_existant:
                            # plus besoin de traiter plus d'annonces car on les a déjà faites dans le passé.
                            faut_il_continuer = 0
                            faut_il_enregistrer_cet_article = faut_il_enregistrer_cet_article * 0
                            if MODE_BAVARD == 1:
                                print("Pas besoin de rechercher plus loin sur internet, car dans le fichier se trouve déjà l'article n° " + str(numero_de_l_annonce))
                            # on sort de la boucle, et on va directement faire un tableau au propre puisqu'on connait
                            break
                            # maintenant le nombre total (nouveaux + anciens)
                    if faut_il_continuer == 0:
                        break
                #------------------------------------------------------------------
                #Trouver la date de l'annonce
                if faut_il_enregistrer_cet_article == 1:
                    mon_paragraphe_suivant4 = art.find_next('aside', {"class":"item_absolute"})
                    mon_paragraphe_suivant5 = mon_paragraphe_suivant4.find('p', {"class":"item_supp"})
                    try:
                        attributs_date_brut = mon_paragraphe_suivant5.attrs #sortir un dictionnaire des attributs du prix
                        ma_date = attributs_date_brut.get("content")
                    except:
                        faut_il_enregistrer_cet_article = 0

                #------------------------------------------------------------------
                # Est-ce que le prix correspond bien à l'article ou est-ce un article sans prix?
                if faut_il_enregistrer_cet_article == 1:
                    if le_lien_du_tag_juste_avant_le_prix != le_lien_du_tag_juste_avant_la_categorie:
                        mon_prix_nettoye = "0"
                
                #------------------------------------------------------------------
                # Vérification de la validité de l'annonce.
                #------------------------------------------------------------------
                # 1 ) Vérification: est ce que le titre de l'annonce contient
                # bien les mots obligatoires ?
                #------------------------------------------------------------------
                if faut_il_enregistrer_cet_article == 1:
                    if verifie_les_imperatifs_dans_le_titre(clean_article_title,  mots_obligatoires_dans_le_titre) == 0:
                        faut_il_enregistrer_cet_article = faut_il_enregistrer_cet_article * 0

                #------------------------------------------------------------------
                # 2 ) est ce que le titre de l'annonce ne contient aucun mot
                # interdit ?
                #------------------------------------------------------------------
                if faut_il_enregistrer_cet_article == 1:
                    if verifie_les_mots_interdits_dans_le_titre(clean_article_title) == 0:
                        faut_il_enregistrer_cet_article = faut_il_enregistrer_cet_article * 0

                #------------------------------------------------------------------
                # 3 ) est ce que le descriptif de l'annonce ne contient aucun
                # mot
                # interdit ?
                #------------------------------------------------------------------
                if faut_il_enregistrer_cet_article == 1:
                    if verifie_les_mots_interdits_dans_le_descriptif(mon_descriptif) == 0:
                        faut_il_enregistrer_cet_article = faut_il_enregistrer_cet_article * 0
                #------------------------------------------------------------------
                # 4 ) est ce que le prix est cohérent?
                #------------------------------------------------------------------
                if faut_il_enregistrer_cet_article == 1:
                    if verifie_la_coherence_du_prix( prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai,  prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf, int(mon_prix_nettoye)) == 0:
                        faut_il_enregistrer_cet_article = 0

                #------------------------------------------------------------------
                # 5 ) est ce que je vais trouver les mots que je souhaite dans le descriptif ou dans le titre Utile pour préciser les recherches de choses qui n'ont pas un numéro de série bien reconnaissable comme un portail pvc de 4m (aucune chance d'avoir ces détails dans le titre)
                #------------------------------------------------------------------
                if faut_il_enregistrer_cet_article == 1:
                    if len( mots_a_trouver_obligatoirement_dans_la_description_ou_le_titre) > 0:
                        if verifie_les_imperatifs_dans_le_titre_et_la_description(clean_article_title + ", " + mon_descriptif,  mots_a_trouver_obligatoirement_dans_la_description_ou_le_titre) == 0:
                            faut_il_enregistrer_cet_article = faut_il_enregistrer_cet_article * 0

                if MODE_BAVARD == 1:
                    print("\n" + clean_article_title + " : " + str(mon_prix_nettoye)+ " : " + str(faut_il_enregistrer_cet_article ))
                #------------------------------------------------------------------
                # remplissage de l'array
                if faut_il_enregistrer_cet_article == 1:
                    # J'enregistre l'article dans mon array.
                    #print(clean_article_text + " " + mon_prix_nettoye + " " +
                    #ma_categorie + " " + ma_ville + " " + mon_lien)
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 0] = clean_article_title
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 1] = int(mon_prix_nettoye)
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 2] = ma_categorie
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 3] = ma_ville
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 4] = mon_lien
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 5] = numero_de_l_annonce
                    my_big_np_array_to_store_and_analyse_results[compteur_pour_la_ligne_de_l_array + suivi_de_la_ligne_a_remplir, 6] = ma_date

                    nombre_d_articles_de_cette_page_enregistres = nombre_d_articles_de_cette_page_enregistres + 1
                    combien_d_elements_dans_mon_array = combien_d_elements_dans_mon_array + 1
                    if MODE_BAVARD == 1:
                        print("Element enregistré: " + str(clean_article_title))
                else:
                    # on n'enregistre rien, j'annule le passage à la ligne suivante de la ligne en dessous.
                    compteur_pour_la_ligne_de_l_array -= 1
                    
                # fin de la boucle:
                compteur_pour_la_ligne_de_l_array += 1

            #j'ajoute le nombre de lignes remplies au suivi global, pour savoir ou stocker les données pour les pages suivantes.
            suivi_de_la_ligne_a_remplir = suivi_de_la_ligne_a_remplir + nombre_d_articles_de_cette_page_enregistres
            #print("fin du traitement de la page " + str(numero_de_la_page_du_bonc))
            numero_de_la_page_du_bonc = numero_de_la_page_du_bonc + 1
        
            #fin de la boucle, passage à l'article suivant

    # pas de mise au propre ni d'enregistrement ni de graphique si rien dans l'array ni dans le fichier enregistré d'archives:
    if ((combien_d_elements_dans_mon_array + len(liste_des_numeros_d_articles_deja_dans_la_base))) != 0:

        #**************************************************************************************************
        # Mise au propre du tableau
        #**************************************************************************************************
        # Compter les annonces valides restantes après le grand tri, et y ajouter le nombre d'articles dans le fichier.

        # trouver ici combien il y a d'articles dans le fichier d'archives, et l'ajouter au nombre d'élément dans l'array de nouveautés.
        mon_array_au_propre = np.empty(((combien_d_elements_dans_mon_array + len(liste_des_numeros_d_articles_deja_dans_la_base)), nombre_d_informations_par_annonce), dtype=object)
        i = 0
        indice_de_l_array_au_propre = -1

        #scanne toutes les lignes de l'array contenant les dernièrestrouvailles.
        for i in range(taille_du_np_array):
            #si c'est une ligne vide, ne rien faire, sinon copier la ligne dans
            #l'array au propre.
            if my_big_np_array_to_store_and_analyse_results[i, 0] != "" and my_big_np_array_to_store_and_analyse_results[i, 0] != None:
                indice_de_l_array_au_propre += 1
                for w in range(0, nombre_d_informations_par_annonce):
                    mon_array_au_propre[indice_de_l_array_au_propre, w] = my_big_np_array_to_store_and_analyse_results[i, w]
    
        i = 0
        # j'ajoute maintenant à l'array au propre le contenu du fichier d'archives pour i de 0 jusqu'au nombre d'articles dans le fichier archive:
        for i in range(len(liste_des_numeros_d_articles_deja_dans_la_base)): #voir ici s'il ne faut pas enlever un.
            indice_de_l_array_au_propre += 1
            # passer en revue chacune des informations
            for w in range(0, nombre_d_informations_par_annonce):
                mon_array_au_propre[indice_de_l_array_au_propre, w] = nparray_importe[i, w]

        if MODE_BAVARD == 1:
            print(mon_array_au_propre)

        #**************************************************************************************************
        # Ecriture de l'array dans le fichier csv au nom adapté
        #**************************************************************************************************
        ## process Unicode text
        #with codecs.open(filename,'w',encoding='utf8') as f:
        #    f.write(text)

        #print("début d'écriture sur le fichier")
        mon_nom_de_fichier = maChaine + ".csv"
        #mise en minuscules du nom de fichier
        mon_nom_de_fichier = mon_nom_de_fichier.lower()
        csvfile = codecs.open(mon_nom_de_fichier, "w+", encoding='utf8')
        writer = csv.writer(csvfile,)
        iteration_des_numeros_de_ligne = 0
        mon_nombre_d_annonces_dans_l_array_final = int(mon_array_au_propre.size) / nombre_d_informations_par_annonce
        for iteration_des_numeros_de_ligne in range(int(mon_nombre_d_annonces_dans_l_array_final)):
            writer.writerow(((mon_array_au_propre[iteration_des_numeros_de_ligne, 0]), (str(mon_array_au_propre[iteration_des_numeros_de_ligne, 1])), (mon_array_au_propre[iteration_des_numeros_de_ligne, 2]), (mon_array_au_propre[iteration_des_numeros_de_ligne, 3]), (mon_array_au_propre[iteration_des_numeros_de_ligne, 4]), (str(mon_array_au_propre[iteration_des_numeros_de_ligne, 5])), (str(mon_array_au_propre[iteration_des_numeros_de_ligne, 6]))))
        try:
            pass
        except:
            print("iteration_des_numeros_de_ligne je n'ai pas pu exporter les données sur le fichier csv")
        finally:
            csvfile.close()

        #**************************************************************************************************
        # Calcul du prix moyen
        #**************************************************************************************************
        somme_des_prix = 0
        pourcentage_considere_comme_une_bonne_affaire = 15
        nombre_d_annonces_a_parcourir = int(mon_array_au_propre.size / nombre_d_informations_par_annonce)
        i = 0
        prix_moyen_national = 0
        for i in range(0, nombre_d_annonces_a_parcourir):
            somme_des_prix = somme_des_prix + int(mon_array_au_propre[i, 1])
        try:
            prix_moyen_national = (somme_des_prix / nombre_d_annonces_a_parcourir)
            prix_d_une_bonne_affaire = int(prix_moyen_national - (prix_moyen_national * pourcentage_considere_comme_une_bonne_affaire / 100))

            Mon_speech = ("\n \n ***********************************************\n Prix moyen national: " + maChaine + " est de " + str(int(prix_moyen_national)) + " €, calculé sur " + str(nombre_d_annonces_a_parcourir) + " articles (ligne verte).\n Cela devient une bonne affaire en dessous de: " + str(prix_d_une_bonne_affaire) + "€ (ligne bleue).")
        except:
            print("\n problème lors du traitement de: " + str(maChaine))
            print("impossible de calculer la moyenne, car cela ferait une division par zéro. Sortie du programme.")
            sys.exit(0)
#**************************************************************************************************
# Faut-il signaler l'objet trouvé (est-ce une bonne affaire)?
#**************************************************************************************************
        i = 0
        ma_liste_des_bonnes_affaires = ""
        au_moins_une_bonne_affaire_trouvee = 0
        # Je passe en revue tous les articles trouvés par la recherche
        for i in range(0, combien_d_elements_dans_mon_array):
            bonne_affaire_trouvee = 0
            #prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai
            #prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf

            # achat de tout objet en dessous d'un prix donné:
            # si le prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai (issu des paramètres de chaque recherche) est négatif, je ne me soucie plus du prix moyen, je renvoie comme "bonne affaire" tout objet dont le prix est inférieur ou égal au prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf
            if int(prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai) ==-1:
                if int(my_big_np_array_to_store_and_analyse_results[i, 1])<= int(prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf) :
                    bonne_affaire_trouvee = 1
            
                    # ici, mettre plutôt un mode d'achat pour chaque article: mode 0, c'est en fonction du prix moyen. mode 1: dès qu'il y en a un en dessous de la limite supérieure donnée. Mode 2: dès que l'objet est entre la limite basse et la limite haute.
                    # pour chaque article, donc, il faudrait un nouveau paramètre pour indiquer quel mode d'achat chosir ci-dessous.
                    # en profiter pour rendre le logiciel user-friendly? saisie des mots à éviter et des options sur une feuille excel, qui transforme le tout en nparray...
                    # En profiter aussi pour mettre pour chaque article qui prévenir et pour chaque personne par quel moyen (email, whatsapp, pushbullet)
            ## achat de tout objet entre deux limites, quelle que soit le prix moyen
            ## si le prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai (issu des paramètres de chaque recherche) est négatif, je ne me soucie plus du prix moyen, je renvoie comme "bonne affaire" tout objet dont le prix est inférieur ou égal au prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf
            #if int(prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai) ==-1:
            #    if int(my_big_np_array_to_store_and_analyse_results[i, 1])<= int(prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf) :
            #        bonne_affaire_trouvee = 1

            # achat de tout objet inférieur au prix calculé d'une bonne affaire
            # sinon, je regarde si le prix de l'article est inférieur ou égal au prix calculé d'une bonne affaire...
            else:
                if int(my_big_np_array_to_store_and_analyse_results[i, 1]) <= int(prix_d_une_bonne_affaire):
                  bonne_affaire_trouvee = 1  

            
            #if 'Morbihan' in my_big_np_array_to_store_and_analyse_results[i, 3] or "Côtesd'Armor" in my_big_np_array_to_store_and_analyse_results[i, 3] or 'IlleetVilaine' in my_big_np_array_to_store_and_analyse_results[i, 3] or 'Finistère' in my_big_np_array_to_store_and_analyse_results[i,3]:
            # si ça se trouve près de chez moi, ça reste une bonne affaire
            # if 'Morbihan' in my_big_np_array_to_store_and_analyse_results[i, 3] or 'IlleetVilaine' in my_big_np_array_to_store_and_analyse_results[i, 3]:
            if 'Morbihan' in my_big_np_array_to_store_and_analyse_results[i, 3]:
                pass
            # si c'est loin de chez moi, ce n'est plus une bonne affaire.
            else:
                bonne_affaire_trouvee = 0

            if bonne_affaire_trouvee == 1 :
                au_moins_une_bonne_affaire_trouvee +=1
                try:
                    #alors je l'ajoute à ma liste des bonnes affaires
                    ma_liste_des_bonnes_affaires = ma_liste_des_bonnes_affaires + ("\nPrix: " + str(my_big_np_array_to_store_and_analyse_results[i, 1]) + "€ (soit " + str(100 - (int((100 * (my_big_np_array_to_store_and_analyse_results[i, 1])) / prix_moyen_national))) + " % sous le prix moyen national). " + my_big_np_array_to_store_and_analyse_results[i, 0] + " catégorie " + my_big_np_array_to_store_and_analyse_results[i, 2] + ". Lieu: " + my_big_np_array_to_store_and_analyse_results[i, 3] + ".\nLien: " + my_big_np_array_to_store_and_analyse_results[i, 4] + "\n")
                    
                    #ouverture directe du lien dans chrome (à ne pas activer si la tâche est masquée
                    # et lancée automatiquement par le planificateur de tâches)
                    #if MODE_BAVARD == 1:
                    #    # ouverture directe de la page de la bonne affaire dans
                    #    ## chrome.
                    #    url = my_big_np_array_to_store_and_analyse_results[i, 4]
                    #    chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
                    #    webbrowser.get(chrome_path).open(url)

                except:
                    print("erreur, l'objet est rien")

        #**************************************************************************************************
        # Calcul du prix ideal de vente
        #**************************************************************************************************
        # Calcul du prix maximum du tableau
        #------------------------------------------------------------------
        somme_des_prix = 0
        nombre_d_annonces_a_parcourir = int(mon_array_au_propre.size / nombre_d_informations_par_annonce)
        i = 0
        prix_maximum_du_tableau = 0
        for i in range(0, nombre_d_annonces_a_parcourir):
            if int(mon_array_au_propre[i, 1]) > prix_maximum_du_tableau:
                prix_maximum_du_tableau = int(mon_array_au_propre[i, 1])
        #------------------------------------------------------------------
        # Création des catégories de prix (quinze catégories)
        #------------------------------------------------------------------
        i = 0
        for i in range(0, nombre_d_annonces_a_parcourir):
            if int(mon_array_au_propre[i, 1]) > prix_maximum_du_tableau:
                prix_maximum_du_tableau = int(mon_array_au_propre[i, 1])
        #------------------------------------------------------------------
        # Creation de l'array qui va contenir les statistiques de prix
        #------------------------------------------------------------------
        # Je compte combien il y a de blocs de 50€ entre zero et le prix
        # maximum
        nombre_de_categories = 40
        valeur_de_chaque_catégorie_de_prix = int((prix_maximum_du_tableau / nombre_de_categories)) + 1
        i = 0
        prix_min = 0
        prix_max = valeur_de_chaque_catégorie_de_prix - 1
        # Je cree l'array avec ce qu'il faut pour y mettre toutes les
        # catégories, plus le nombre d'occurrences dans chaque catégorie de
        # prix.
        mon_array_statistiques = np.empty((nombre_de_categories, 2), dtype=object)
        # Je nomme les catégories et je remplis les valeurs correspondantes de
        # zéros
        for i in range(nombre_de_categories):
            mon_array_statistiques[i, 0] = 'de ' + str(prix_min) + ' à ' + str(prix_max) + " : "
            mon_array_statistiques[i, 1] = 0
            prix_min = prix_min + valeur_de_chaque_catégorie_de_prix
            prix_max = prix_max + valeur_de_chaque_catégorie_de_prix

        #------------------------------------------------------------------
        # Comptage du nombre d'annonces dans chaque catégorie
        #------------------------------------------------------------------
        nombre_d_annonces_a_parcourir = int(mon_array_au_propre.size / nombre_d_informations_par_annonce)
        i = 0
        mon_prix_a_classer = 0
        for i in range(0, nombre_d_annonces_a_parcourir):
            mon_prix_a_classer = int((int(mon_array_au_propre[i, 1]) / valeur_de_chaque_catégorie_de_prix))
            mon_array_statistiques[mon_prix_a_classer, 1] += 1
        if MODE_BAVARD == 1:
            print(mon_array_statistiques)
        #------------------------------------------------------------------
        # Détermination du prix critique (analyse fine du "bon" prix via la courbe des prix à réaliser)
        #------------------------------------------------------------------

        #**************************************************************************************************
        # graphique
        #**************************************************************************************************
        i = 0
        la_plus_forte_valeur = 0
        x = np.arange(len(mon_array_au_propre))
        s = (len(mon_array_au_propre))
        array_des_prix = np.zeros(s)
        # Peupler l'array y avec tous les prix, et déterminer par la même
        # occasion le prix le plus fort pour
        # savoir comment dessiner l'axe des ordonnées.
        for mon_article_au_propre in mon_array_au_propre:
            array_des_prix[i] = int(mon_article_au_propre[1])
            if la_plus_forte_valeur < int(mon_article_au_propre[1]):
                la_plus_forte_valeur = int(mon_article_au_propre[1])
            i += 1

        fig = plt.figure()
        fig.suptitle('Prix moyen national ' + maChaine + ': ' + str(int(prix_moyen_national)) + " €\nAffaire (-" + str(pourcentage_considere_comme_une_bonne_affaire) +"%): " + str(prix_d_une_bonne_affaire) + " €.", fontsize=12, fontweight='bold')
        axis = fig.add_subplot(111)
        fig.subplots_adjust(top=0.85)
        axis.set_ylabel('prix')
        axis.axis([0, len(mon_array_au_propre), 0, int(la_plus_forte_valeur + 20)])
        axis.plot(x, array_des_prix, 'ro')
        axis.add_artist(lines.Line2D((0, len(mon_array_au_propre)), (prix_moyen_national, prix_moyen_national), color = 'green'))
        axis.add_artist(lines.Line2D((0, len(mon_array_au_propre)), (prix_d_une_bonne_affaire, prix_d_une_bonne_affaire), color = 'blue'))

        axis.annotate((str(int(prix_moyen_national)) + " €."), xy=(len(mon_array_au_propre), prix_moyen_national), xytext=(len(mon_array_au_propre), prix_moyen_national))
        axis.annotate((str(int(prix_d_une_bonne_affaire)) + " €."), xy=(len(mon_array_au_propre), prix_d_une_bonne_affaire), xytext=(len(mon_array_au_propre), prix_d_une_bonne_affaire))
        # affiche le graphique
        #plt.show()

        #enregistre le graphique des prix au nom du fichier dans le répertoire
        #de la solution.
        fig.savefig(maChaine + ".jpg")
        plt.close('all') #ferme tous les graphiques

        if au_moins_une_bonne_affaire_trouvee > 0:
            # préparation du tableau statistique à l'envoi
            str_mon_tableau_statistique = "\nNombre d'articles en vente dans les différentes catégories de prix:\n"
            for x in mon_array_statistiques:
                str_mon_tableau_statistique = str_mon_tableau_statistique + str(x[0]) + " " + str(x[1]) + "\n"
            
            # -------------------------envoi des notifications--------------------
            # préparation du message            
            message_a_envoyer = ma_liste_des_bonnes_affaires + "\n" + Mon_speech + " " + str_mon_tableau_statistique

            # affichage de la bonne affaire dans la fenêtre interactive
            print(message_a_envoyer)

            # envoi du message email
            email_me.send_email(maChaine, message_a_envoyer, maChaine + ".jpg")
            
            # envoi d'une notification pushbullet
            print(notification_pushbullet.envoi_notification_pushbullet(maChaine, message_a_envoyer))

            ## envoi d'un sms
            #print(notification_pushbullet.envoi_sms_pushbullet(message_a_envoyer, "+33623665676"))
    else:
        print("Aucun résultat cette fois pour: " + str(maChaine) + ".")

#***************************************************************************************************************************************************
#                                                               DEBUT DU PROGRAMME                                                    
#***************************************************************************************************************************************************
MES_RECHERCHES = liste_des_recherches.importe_mes_recherches()

#**************************************************************************************************
# Paramètres du programme: définir ce que l'on cherche
#**************************************************************************************************
ma_liste_d_arrays_statistiques = []
for chaque_recherche in range(len(MES_RECHERCHES)):
    maChaine = ""
    taille_du_np_array = 12000 # a éclaircir
    #attention, si on modifie le nombre ci-dessous, il faut aussi aller modifier la lecture et l'écriture des fichiers dans les fonctions enregistrement_des_resultats_dans_un_fichier_csv ...
    nombre_d_informations_par_annonce = 7 # 0: titre.  1: prix (int).  2: catégorie.  3: lieu.  4: lien.  5: identifiant.  6: date 

    mots_de_la_recherche = MES_RECHERCHES[chaque_recherche][0]
    mots_interdits_dans_le_titre = MES_RECHERCHES[chaque_recherche][1]
    mots_non_souhaites_dans_le_descriptif = MES_RECHERCHES[chaque_recherche][2]
    prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai = MES_RECHERCHES[chaque_recherche][3][0]  #attention, string
    prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf = MES_RECHERCHES[chaque_recherche][3][1]  #attention, string
    mots_obligatoires_dans_le_titre = MES_RECHERCHES[chaque_recherche][4]
    mots_a_trouver_obligatoirement_dans_la_description_ou_le_titre = MES_RECHERCHES[chaque_recherche][5]
    try:
        url_fourni = MES_RECHERCHES[chaque_recherche][6][0]  #s'il n'y a aucun élément dans la liste, cette valeur renvoie une erreur, d'où le try.
    except:
        url_fourni = ""
    for mon_mot in  mots_de_la_recherche:
        maChaine = maChaine + " " + mon_mot
        maChaine = maChaine.lstrip()
    mon_chrono_au_depart_du_scan = time.clock()
    _ = scanne_archive_analyse_et_graphique(maChaine, prix_en_dessous_duquel_c_est_trop_beau_pour_etre_vrai,  prix_au_dessus_duquel_c_est_plus_cher_que_le_neuf,  mots_obligatoires_dans_le_titre,  mots_a_trouver_obligatoirement_dans_la_description_ou_le_titre, url_fourni)
    mon_chrono_a_la_fin_du_scan = time.clock()
    mon_chrono = mon_chrono_a_la_fin_du_scan-mon_chrono_au_depart_du_scan
    if mon_chrono > 5:
        synthese_du_controle_des_durees = synthese_du_controle_des_durees + "\n" + maChaine +" : "+ str(round(mon_chrono, 1)) + "\n"
#mixer.init()
#mixer.music.load('F:/Musique/drop.mp3')
#mixer.music.play()
chrono_fin_du_programme = time.clock()
temps_total_du_programme_en_secondes = int(chrono_fin_du_programme-chrono_debut_du_programme)
temps_total_du_programme_en_texte = str(int(temps_total_du_programme_en_secondes/60)) + " minutes et " + str(temps_total_du_programme_en_secondes % 60) + " secondes"
synthese_chrono_total = "\nDurée totale du programme: " + temps_total_du_programme_en_texte

#if 1 == 1:
if date.hour ==7 and date.minute > 45:
    email_me.send_email("Robot LeBonCoin OK. ", "Le robot crawler fonctionne correctement." + synthese_du_controle_des_durees + synthese_chrono_total+ "\nNombre d'articles recherchés: " + str(len(MES_RECHERCHES)), "")
print("Recherche terminée sans erreur.")
