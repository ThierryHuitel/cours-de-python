from bs4 import BeautifulSoup
import numpy as np
import requests
# premi�re page du bonc
link_to_explore = 'https://www.leboncoin.fr/annonces/offres/bretagne/occasions/?th=1&q=%22galaxy%20S3%20mini'
# derni�re page du bonc
#link_to_explore = 'https://www.leboncoin.fr/annonces/offres/bretagne/occasions/?o=9&q=%22galaxy%20S3%20mini'
website_response = requests.get(link_to_explore)
html_from_the_website = website_response.text
#print(html)   #prints all the html of the page !
soup= BeautifulSoup(html_from_the_website, 'html.parser')
print("debug1")


#  ????????? Nettoyage d'un fichier html pour en extraire le texte ?????????
def strip_tags(html_to_process, invalid_tags):
    """Remove from the given html code all the specified tags.

    html_to_process: contains the html code to clean

    invalid_tags: liste des tags � enlever du code 
        example: 
        invalid_tags = ['b', 'i', 'u']
    """
    internal_soup = BeautifulSoup(html_to_process, 'html.parser')

    for tag in internal_soup.findAll(True):
        if tag.name in invalid_tags:
            s = ""

            for c in tag.contents:
                if not isinstance(c, NavigableString):
                    c = strip_tags(str(c), invalid_tags)
                s += str(c)

            tag.replaceWith(s)
    return internal_soup



articles_title = (soup.find_all("h2",{"class":"item_title"})) #renvoie tous les paragraphes h2, de la bonne classe du document.
articles_price = (soup.find_all("h3",{"class":"item_price"})) #renvoie le prix des articles

#Cr�e un array numpy avec autant de lignes que d'articles, et 4 colonnes (ou linverse car je ne sais pas encore trop)
my_big_np_array_to_store_and_analyse_results = np.empty((len(articles_title), 2), dtype=object) 

invalid_tags = ['b', 'i', 'u']
i=0
for art in articles_title:
    # Remove unwanted tags from html.
    dirty_articles_tags = art.get_text()
    cleaned_article_tags = strip_tags(dirty_articles_tags, invalid_tags)
    # Remove unwanted spaces from the text of the tag.
    clean_article_text= cleaned_article_tags.text.strip()
    # Remove unwanted things from the text
    # clean_article_text= clean_article_text.strip('xa0�')
    my_big_np_array_to_store_and_analyse_results [i, 0] =clean_article_text
    #print(article_nettoy�)
    i+=1

j=0
for prix in articles_price:
    dirty_prices_tags = prix.get_text()
    cleaned_prices_tags = strip_tags(dirty_prices_tags, invalid_tags)
    # Remove unwanted spaces from the text of the tag.
    clean_price_text= cleaned_prices_tags.text.strip()
    # Remove unwanted things from the text
    clean_price_text= clean_price_text.strip('\xa0�')

    my_big_np_array_to_store_and_analyse_results [j, 1] = int(clean_price_text)
    j+=1

#print(my_big_np_array_to_store_and_analyse_results)
print("debug2")
#**************************************************************************************************


#  ????????? Nettoyage d'un fichier html pour en extraire le texte ?????????
#soup.find(id="next")
#print((soup.find(id="next")).get('href'))  

foo = soup.find(class_="element page static")
print("debug3")
print(foo.text)
#soup.find(id='next').find(class_="element page static").p.a.get('href')
#for link in soup.find_all('a'):    
   # print(link.get('href'))
#ma_page_suivante = soup.find(id="next")
#<a href="//www.leboncoin.fr/annonces/offres/bretagne/occasions/?o=2&amp;q=one%20plus%203T" class="element page static" id="next">
# This div contains the article's body

##  ????????? Nettoyage d'un fichier html pour en extraire le texte ?????????
#    # (June 2017 Note: Body nested in two div tags)
#    content_div = soup.find(id="mw-content-text").find(class_="mw-parser-output")

#    # stores the first link found in the article, if the article contains no
#    # links this value will remain None
#    article_link = None

#    # Find all the direct children of content_div that are paragraphs
#    for element in content_div.find_all("p", recursive=False):
#        # Find the first anchor tag that's a direct child of a paragraph.
#        # It's important to only look at direct children, because other types
#        # of link, e.g. footnotes and pronunciation, could come before the
#        # first link to an article. Those other link types aren't direct
#        # children though, they're in divs of various classes.
#        if element.find("a", recursive=False):
#            article_link = element.find("a", recursive=False).get('href')
#            break

#    if not article_link:
#        return 

#    # Build a full url from the relative article_link url
#    first_link = urllib.parse.urljoin('https://en.wikipedia.org/', article_link)

#    return first_link


##  ????????? Nettoyage d'un fichier html pour en extraire le texte ?????????