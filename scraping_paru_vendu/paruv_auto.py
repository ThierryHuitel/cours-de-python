from selenium import webdriver
import numpy as np 
import pandas as pd
from time import sleep, time
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
def recup(depart):
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    navigateur = webdriver.Chrome(executable_path="D:\Documents\Repos\cours-de-python\scraping_paru_vendu\chromedriver.exe", options=options)
    navigateur.get(depart)
    sleep(5)

    navigateur.find_element_by_tag_name('button').click()
    navigateur.find_element_by_link_text('Auto-Moto').click()
    navigateur.find_element_by_link_text("RÉGIONS").click()
    navigateur.find_element_by_link_text("Centre").click()
    
    sleep(3)
    
    carac = []
    c=0
    condition =True
    while condition:
        sleep(3)
    
        try:
            elements= navigateur.find_elements_by_class_name('lazyload_bloc')
        except ElementClickInterceptedException:
            navigateur.find_element_by_partial_link_text('suivante').click()
            
    
        for i in range(len(elements)):
            modele = elements[i]
            
            try:
                type_vendeur=navigateur.find_element_by_class_name('pseudoinfo').text
            except NoSuchElementException:
                type_vendeur='Particulier'
                 
            modele.click()
            sleep(3)
            
            try:
                nom=navigateur.find_element_by_class_name('auto2012_dettophead1txt1').text
            except NoSuchElementException:
                nom=''
            
            try:
                price=navigateur.find_element_by_class_name('px')
                prix=price.find_element_by_tag_name('span').text
            except NoSuchElementException:
                prix=""
        
            try:
                kil=navigateur.find_element_by_class_name('kil')
                kilo=kil.find_element_by_tag_name('span').text
            except NoSuchElementException:
                kilo=""
            
            try:
                ener=navigateur.find_element_by_class_name('en')
                energie=ener.find_element_by_tag_name('span').text
            except NoSuchElementException:
                energie=""
        
            try:
                pers=navigateur.find_element_by_class_name('por')
                places=pers.find_element_by_tag_name('span').text
            except NoSuchElementException:
                places=""
        
            try:
                co=navigateur.find_element_by_class_name('emiss')
                co2=co.find_element_by_tag_name('span').text
            except NoSuchElementException:
                co2=""
            
            try:
                conso=navigateur.find_element_by_class_name('cons')
                consom=conso.find_element_by_tag_name('span').text
            except NoSuchElementException:
                consom=""
             
            try: 
                trans=navigateur.find_element_by_class_name('vit')
                transmission=trans.find_element_by_tag_name('span').text
            except NoSuchElementException:
                transmission=""
        
            try:
                kil=navigateur.find_element_by_class_name('kil')
                kilo=kil.find_element_by_tag_name('span').text
            except NoSuchElementException:
                kilo=""
        
            try:
                nbrp=navigateur.find_element_by_class_name('carro')
                portes=nbrp.find_element_by_tag_name('span').text
            except NoSuchElementException:
                portes=""
        
            try:
                puiss=navigateur.find_element_by_class_name('puiss')
                cv=puiss.find_element_by_tag_name('span').text
            except NoSuchElementException:
                cv=""
            
            try:
                ann=navigateur.find_element_by_class_name('ann')
                annee=ann.find_element_by_tag_name('span').text
            except NoSuchElementException:
                annee=""

          
            navigateur.back()
            elements= navigateur.find_elements_by_class_name('lazyload_bloc')
        
            c=c+1
            
            features = {
                'Type_vendeur':type_vendeur,
                'Modele':nom,
                'Prix':prix,
                'Kilométrage':kilo,
                'Energie':energie,
                'Capacité':places,
                'Emission_CO2':co2,
                'Consommation':consom,
                'Transmission':transmission,
                'Capacité':places,
                'Nombres de portes':portes,
                'Chevaux fiscaux':cv,
                'Année':annee
            }
            
            carac.append(features)
        
    
        if c < 2500 :
            try:
                navigateur.find_element_by_partial_link_text('suivante').click()
            except ElementClickInterceptedException:
                navigateur.close()
        else:
            condition=False
        
    return carac
depart = 'https://www.paruvendu.fr/'
def creation(depart):
    donnees=recup(depart)
    df=pd.DataFrame(donnees)
    df.to_csv('ParuVendu.csv')
    return df
creation(depart)