import xlwings as xw # nécessaire pour l'import des données excel import_an_excel_sheet_into_a_python_array
import os # necessary for scan_this_folder_for_that_kind_of_files

def import_an_excel_sheet_into_a_python_array():    
    mon_array = xw.Range("A1:F4").value
    print(mon_array)

def scan_this_folder_for_that_kind_of_files(path, file_extension):
    """ provide path (beware, use / instead of \ ) and one or several file extensions
        use example: scan_this_folder_for_that_kind_of_files('c:/dropbox/', (".html", ".htm", "doc"))
    """
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith(file_extension):
                print(name)

def scan_this_folder_for_that_kind_of_files_and_put_them_in_a_list(path, file_extension):
    """ provide path (beware, use / instead of \ ) and one or several file extensions
        this function returns a list with all the files (and their path) that have the wanted extension.
        use example: print(scan_this_folder_for_that_kind_of_files_and_put_them_in_a_list('c:/dropbox/', (".html", ".htm", "doc")))
        returns: ['c:/dropbox/2014 Imprimé congés.doc', 'c:/dropbox/bookmarks.html']
    """
    wanted_files_list = [os.path.join(root, name)
                 for root, dirs, files in os.walk(path)
                 for name in files
                 if name.endswith(file_extension)]
    return wanted_files_list

