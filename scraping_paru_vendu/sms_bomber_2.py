from pushbullet import API

# set the API Class and link it to the api object
api = API()

# now set the token
access_token = "o.PnuQgn4Yk9opX8AbAxNtNXl89TeFqXdE"
api.set_token(access_token)

# now you can use the API Wrapper

# sending a note, arguments: title, message
api.send_note(
    "Salut",
    "Essai 2 from the Pushbullet Python API wrapper.")

# # sending a link, arguments: title, message, url
# api.send_link("A Link", "Here is my website.", "http://www.lingeswaran.com")

# sending a sms, arguments: addresses, file_type, guid, sms_message, target_device
response = api.send_sms("+33766353231", "Essai de SMS par python", "ujyZggnTBuusjDZcf6HhO8")
print (response)


# sending a file, arguments: title, message, file name, file type, file url
# api.send_file("An Example Image", "Check out the example image!",
#               "test_image.png", "image/png",
#               "https://dummyimage.com/600x400/000/fff")
# import requests

# headers = {
#     'Access-Token': 'o.UGJWKsDKSTnYYJMvW2toHSHtkrEj6NC3',
# }

# json_data = {
#     'data': {
#         'addresses': [
#             '+33766353231',
#         ],
#         'file_type': 'image/jpeg',
#         'guid': '993aaa48567d91068e96c75a74644159',
#         'message': 'Text message body.',
#         'target_device_iden': 'ujpah72o0sjAoRtnM0jc',
#     },
#     'file_url': 'https://dl.pushbulletusercontent.com/foGfub1jtC6yYcOMACk1AbHwTrTKvrDc/john.jpg',
# }

# response = requests.post('https://api.pushbullet.com/v2/texts', headers=headers, json=json_data)