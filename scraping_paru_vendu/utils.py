from bs4 import BeautifulSoup
import numpy as np
import requests
import csv # pour écrire dans un fichier csv

import io 
import functools #pour décoder les fichiers entrants
import sys #pour décoder les fichiers entrants
import codecs #pour encoder et décoder les fichiers (et les avoir avec les accents corrects
# Import smtplib for the actual sending function
import smtplib
#*************************************************************
# How to scrape dynamically updated information from a webpage
#*************************************************************
# Parfois, on ne voit pas les données qui sont dynamiquement affichées par du javascript,
# tout simplement parce qu'on n'est pas un client qui exécute du code javascript
# Il faut donc faire comme si on était un navigateur internet pour executer ce code javascript
# L'outil pour cela est Qt5

import sys #parce que nous allons utiliser Qt, qui va demander des infos au système, et qui ne sera pas content s'il ne les a pas.
# QApplication va servir à créer l'application:
from PyQt6.QtWidgets import QApplication # cela se trouve dans QtGui dans les anciennes versions
from PyQt6.QtCore import QUrl # Pour lire l'URL
# QWebPage va nous permettre de vraiment charger la page, agir comme un client, 
# agir comme un navigateur, et jouer le code javascript
from PyQt6.QtWebKitWidgets import QWebPage #cela se trouve dans QtWebKit dans les anciennes versions.

import bs4 as bs
import urllib.request

# On va créer cette classe Client, qui va hériter de QWebPage
class Client(QWebPage):
    
    def __init__(self, url):
        #on définit l'application
        self.app = QApplication(sys.argv)
        #on initialise cette QWebPage
        QWebPage.__init__(self)
        # on connecte la méthode "self.on_page_load" quand le chargement est terminé
        # c'est nécessaire car PyQt est une bibliothèque asynchrone, et que
        # si on voulait utiliser directement QWebPage, il n'y aurait rien dedans
        # car la page ne se serait pas chargée: QWebPage nous donnerait tout de suite la réponse sans 
        # attendre la fin du chargement de la page.
        self.loadFinished.connect(self.on_page_load)
        # On charge QUrl, qui lui même va charger l'URL
        self.mainFrame().load(QUrl(url))
        # et là, l'application s'execute.
        self.app.exec_()
    
    # on veut juste que le client fonctionne le temps que la page se charge.
    # ensuite, on n'en a plus besoin, donc on ferme l'application:
    def on_page_load(self):
        self.app.quit()
        
url = "https://www.paruvendu.fr/mondebarras/listefo/default/default/?fulltext=Galaxy&elargrayon=1&ray=50&idtag=1795&r=BMUTT000&libelle_lo=Vannes+%2856000%29&codeinsee=56260&lo=56000&pa=&ray=50&zvy=&zvt=&px0=&px1=&zmd%5B%5D=VENTE&zmd%5B%5D=TROC&zmd%5B%5D=DON&zmd%5B%5D=RECH&zep%5B%5D=&codPro=off&filtre=&tri=&ck-part=on"

client_response = Client(url)
# client_response hérite de QWebPage. On peut donc utiliser les méthodes de QWebPage maintenant
# C'est ce qu'on fait avec mainFrame (on récupère le contenu principal, et on le convertit en html.)
source = client_response.mainFrame().toHtml() 
soup = bs.BeautifulSoup(source, "lxml")
#js_test=soup.find("div", class_="section-directions-trip-distance section-directions-trip-secondary-text")
#print(js_test.text)
ma_liste_des_tags = []

#**************************************************************************************************
# Pour chaque tag de ce fichier html, imprimer le nom du tag (attribut), et le texte qu'il contient
#**************************************************************************************************
for tag in soup.find_all(True):
    if tag.string != None:
        print(tag.attrs)
        ma_liste_des_tags.append(tag.string)
        a=1


#**************************************************************************************************
# Ecrire tout cela dans un fichier csv, car les longues pages ne s'afficheront pas toutes
# sur l'écran python, et car ce sera plus facile pour trouver la chaine de caractère que l'on veut
#**************************************************************************************************

        #print("début d'écriture sur le fichier")
mon_nom_de_fichier = "mon_html_decortique.csv"
csvfile=codecs.open(mon_nom_de_fichier, "w+", encoding='utf8')
writer=csv.writer(csvfile, )

for tag in soup.find_all(True):
    if tag.string != None:
        writer.writerow(tag.attrs)
        writer.writerow(tag.string)
        writer.writerow("")
        a=1
csvfile.close()