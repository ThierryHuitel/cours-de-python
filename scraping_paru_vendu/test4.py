import numpy as np
from openpyxl import load_workbook
import time                         # pour chronométrer chaque recherche
# exemple:
#charge_les_donnees_du_fichier_excel('JobTiti.xlsx', 'job', '2', '1', '6', '3')
# numpy de 5*3
chrono_debut_du_programme = time.clock()
synthese_chrono_total = "\n Durée totale du programme: "

def charge_les_donnees_du_fichier_excel(nom_du_fichier, nom_de_la_feuille, ligne_min, col_min, ligne_max, col_max):
    """
    cette fonction va ouvrir le fichier excel fourni en argument (ce fichier doit se trouver dans le répertoire du projet)
    puis sélectionner la page choisie
    puis prendre les données dans la zone définie par les arguments
    Toutes ces données sont mises dans un numpy array qui est renvoyé par la fonction.
    exemple d'utilisation:
    charge_les_donnees_du_fichier_excel('JobTiti.xlsx', 'job', '2', '1', '6', '3')
    va scanner les trois colonnes de la ligne 2, puis les trois colonnes de la ligne 3, etc jusqu'à la ligne 6.
    cela renvoie:
    [['médecin' 'B' 'médecin']
     ['ingénieur' 'C' 'ingénieur']
     ['professeur' None 'professeur']
     ['comptable' None 'comptable']
     ['pharmacien' None 'pharmacien']]
    """
    try:
        nombre_de_lignes_de_l_array = (int(ligne_max)+1)-(int(ligne_min))
        nombre_de_colonnes_de_l_array = ((int(col_max)+1)-(int(col_min)))
        mon_array_de_donnees_issues_d_excel = np.empty([nombre_de_lignes_de_l_array,nombre_de_colonnes_de_l_array], order='C')
        mon_array_vide = mon_array_de_donnees_issues_d_excel
        wb = load_workbook(filename=nom_du_fichier, read_only=False)
        ws=wb.get_sheet_by_name(nom_de_la_feuille)
        
        ligne_en_cours = 0
        for row in ws.iter_rows(min_row=int(ligne_min), min_col=int(col_min), max_row=int(ligne_max), max_col=int(col_max)):
            colonne_en_cours = 0
            for cell in row:
                mon_array_de_donnees_issues_d_excel[ligne_en_cours, colonne_en_cours]= cell.value
                colonne_en_cours +=1
            ligne_en_cours +=1
        #print(mon_array_de_donnees_issues_d_excel)
        # Save the file
        # wb.save("test.xlsx")
        return mon_array_de_donnees_issues_d_excel
    except:
        print("une erreur s'est produite dans la manipulation du fichier")
        return None
    

def ajoute_ces_donnees_a_la_premiere_colonne_de_ce_fichier_excel(nom_du_fichier, nom_de_la_feuille, donnees_a_ajouter_a_la_premiere_colonne):
    """
    cette fonction va ouvrir le fichier excel fourni en argument (ce fichier doit se trouver dans le répertoire du projet)
    puis sélectionner la page choisie
    ... à compléter
    """
    try:
        wb = load_workbook(filename=nom_du_fichier, read_only=False)
        ws=wb.get_sheet_by_name(nom_de_la_feuille)
        
        for row in donnees_a_ajouter_a_la_premiere_colonne:
            my_list = []
            my_list.append(row)
            ws.append(my_list)
        # Save the file
        wb.save(nom_du_fichier)
    except:
        print("une erreur s'est produite dans la manipulation du fichier")
    

## Data can be assigned directly to cells
#ws['A30'] = 42

## Rows can also be appended
#ws.append([1, 2, 3])

## Python types will automatically be converted
#import datetime
#ws['A22'] = datetime.datetime.now()

    
# Chargement des mots interdits qui sont stockés dans un fichier excel dans le même répertoire.
#try:
# chiffres dans l'ordre: ligne de la première cellule, colonne de la première cellule, puis ligne et colonne de la dernière cellule.
recuperation_des_parametres = charge_les_donnees_du_fichier_excel('BoiteAClousBis.xlsx', 'Sommes', '2', '2', '3', '2')
mon_array_de_donnée = recuperation_des_parametres[0]
ma_valeur_recherchée =mon_array_de_donnée[0]
mon_array_de_nombre_de_donnees = recuperation_des_parametres[1]
mon_nombre_de_données = mon_array_de_nombre_de_donnees[0]
mon_tableau_des_valeurs_a_examiner = charge_les_donnees_du_fichier_excel('BoiteAClousBis.xlsx', 'Sommes', '1', '1', str(int(mon_nombre_de_données)), '1')
#except:
#    print("erreur dans le chargement du tableau excel des interdits. Le programme ne peut pas poursuivre son exécution")
#    exit

#**************************************************************************************************
# Récupération des données du site internet
#**************************************************************************************************
# Chercher quelles valeurs ajoutées forment la valeur cherchée
valeur_recherchée = float(ma_valeur_recherchée)
print ("Voici les combinaisons de valeurs qui font " + str(valeur_recherchée) + " :")
# La liste des valeurs
maliste = mon_tableau_des_valeurs_a_examiner
# la taille de la liste
taille_de_ma_liste = len(maliste)
# un compteur binaire
choice = [False] * taille_de_ma_liste
# pour chacune des 2^N valeurs du compteur binaire
mon_compteur = 2**taille_de_ma_liste
for k in range (0, mon_compteur):
    print(k)
    # somme des valeurs pour lesquelles le bit du compteur binaire est à "1"
    sum=0
    for i in range (0, taille_de_ma_liste-1):
        if choice[i] == True:
            sum+=maliste[i]
    # affiche l'opération effectuée et son résultat
    for i in range (0, taille_de_ma_liste-1):
        if sum == valeur_recherchée:
            if choice[i] == True:
                print(str(maliste[i]) + " ", end= " ")
    if sum == valeur_recherchée:
        #print ("= " + str(sum))
        print (" ")
    # incrémentation du compteur binaire
    for i in range (0, taille_de_ma_liste-1):
        if choice[i] == False:
            choice[i] = True
            break
        choice[i] = False

chrono_fin_du_programme = time.clock()
temps_total_du_programme_en_secondes = int(chrono_fin_du_programme-chrono_debut_du_programme)
temps_total_du_programme_en_texte = str(int(temps_total_du_programme_en_secondes/60)) + " minutes et " + str(temps_total_du_programme_en_secondes % 60) + " secondes"
synthese_chrono_total = "\nDurée totale du programme: " + temps_total_du_programme_en_texte + " pour " + str(mon_array_de_nombre_de_donnees) + " éléments"
print(synthese_chrono_total)