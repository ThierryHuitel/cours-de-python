""" Ce module renvoie une taquinerie gentille choise aléatoirement dans une liste"""
import random

def cree_un_petit_mot_gentil():
    """Cette fonction va renvoyer une gentillesse
    qu'elle aura choisi aléatoirement dans une liste.

    Paramètres:
    -----------
    Aucun

    Renvoie:
    --------
    Un message de taquinerie au format Str
    """
    ma_liste_de_gentillesses = ["On dirait que t'es pas en forme ce matin...",
                                "As-tu bien les yeux en face des trous? ",
                                "Non mais allô, quoi! "]
    la_gentillesse_choisie = random.choice(ma_liste_de_gentillesses)
    return la_gentillesse_choisie


def cree_un_petit_mot_mechant():
    """Cette fonction va renvoyer une mechanceté
    qu'elle aura choisi aléatoirement dans une liste.

    Paramètres:
    -----------
    Aucun

    Renvoie:
    --------
    Un message méchant au format Str
    """
    liste_mechancetes = ["Toi, t'as été berçé trop près du mur!",
                        "Le jour où les cons feront des étincelles, t'as intérêt de fermer le gaz!",
                        "le jour où les cons mangeront de l'herbe, on pourra revendre ta tondeuse",
                        "Le jour où les cons auront du flair, tu vas trouver des truffes"]
    la_mechancete_choisie = random.choice(liste_mechancetes)
    return la_mechancete_choisie


def cree_une_insulte():
    """Cette fonction va renvoyer une insulte
    qu'elle aura choisi aléatoirement dans une liste.

    Paramètres:
    -----------
    Aucun

    Renvoie:
    --------
    Une insulte au format Str
    """
    ma_liste_d_insultes = ["Abruti",
                                "Bâtard",
                                "Bouffon",
                                "Mongol",
                                "Fils de moine",
                                "Pue la pisse",
                                "Tête de noeud"]
    l_insulte_choisie = random.choice(ma_liste_d_insultes)
    return l_insulte_choisie

"""
print(cree_une_insulte())
print(cree_un_petit_mot_gentil())
print(cree_un_petit_mot_mechant())
"""