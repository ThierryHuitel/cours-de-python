#!/bin/bash
Help()
{
   echo "Script de création d'un service à partir d'un script python."
   echo
   echo "Syntax: ./create_service <script_python>"
   echo
   echo "    Avec script_python = nom du fichier de script python, avec un chemin du répertoire si besoin (chemin complet, absolu ou relatif) "
   echo
}

if [ "$#" -ne 1 ]; then
    Help
    echo "Paramètre script_python manquant"
    exit 1
fi

script_path="$1"
script_path=$(realpath $script_path)

if [ ! -f "$script_path" ]
then
    echo "Le chemin vers le script python à lancer en service $script_path est invalide, échec de création de service"
    echo "Conseil: utiliser l'auto-complétion avec TAB en saisissant le paramètre script_path pour s'assurer de la validité du chemin"
    exit 1
fi

# Récupération du nom du service à créer à partir du nom du fichier de script python (nom sans extension)
# Suppression du chemin
service_name="${script_path##*/}"
# Suppression de l'extension
service_name="${service_name%.py}"
echo "Nom de service extrait du nom de script python: $service_name"
if [ "" = "$service_name" ]
then
    echo "Erreur de récupération de nom de service à partir du nom de fichier"
    exit 1
fi
if [ -f "/etc/systemd/system/$service_name.service" ];
then
	echo "Service existant, il sera écrasé"
fi
 
echo "Configuration de service: /etc/systemd/system/$service_name.service"
echo "[Unit]
Description=Script python de titi
After=multi-user.target
[Service]
Type=simple
Restart=always
WorkingDirectory=$HOME
ExecStart=/usr/bin/python3 $script_path
[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/$service_name.service > /dev/null

sudo systemctl daemon-reload
sudo systemctl enable $service_name.service
sudo systemctl start $service_name.service
sleep 1
sudo systemctl status $service_name.service
