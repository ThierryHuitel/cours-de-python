"""Mot d'accueil

Ce petit programme va accueillir comme il se doit le visiteur.
"""
import recueil_de_poesie

ton_prenom = input("Quel est ton prénom ? ")
print(f"Salut {ton_prenom}! {recueil_de_poesie.cree_un_petit_mot_gentil()}")
print(f"Eh ben {ton_prenom}, t'es un {recueil_de_poesie.cree_une_insulte()}")
